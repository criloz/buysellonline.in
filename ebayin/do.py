'''
Created on 19/03/2012

@author: root
'''
import os,sys,json,base64

axd_axd = os.path.realpath(__file__).replace("/ebayin/do.py","/")
sys.path.append(axd_axd+"scraper")
sys.path.append(os.path.dirname(__file__))

import cgi
from manager.coreweb import *

def application(environ,start_response):
    pythonpath = '/usr/local/python2.7/bin/python'
    script  = os.path.realpath(__file__).replace("do.py","ebay_extractor.py")
    form = cgi.FieldStorage(fp=environ['wsgi.input'], environ=environ)
    request = {}
    for i in form:
        request[i] = form.getvalue(i, None)
    if len(request)>0:
        try: 
            output = str(os.spawnv(os.P_NOWAIT,pythonpath,('python',script,base64.b64encode(json.dumps(request)))))
            output = output + "<br>ok" #+base64.b64encode(json.dumps(request))
        except:
            output = "Bad Request"
    else:
        output = "NO Request"
       
    status = '200 OK'
    output =   output.encode('utf-8') 
    response_headers = [('Content-type', 'text/html; charset=utf-8'),
                        ('Content-Length', str(len(output)))]
    start_response(status, response_headers)

    return [output]

