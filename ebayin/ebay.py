'''
Created on 28/03/2012


@author: cristian criscalovis@gmail.com

'''

from scrapper.core.proxies import urlopen 
from scrapper.BeautifulSoup import BeautifulSoup
import re

def remove_tags(soup):
    VALID_TAGS = ''
    for tag in soup.findAll(True):
        if tag.name not in VALID_TAGS:
            for i, x in enumerate(tag.parent.contents):
                if x == tag: break
            else:
                print "Can't find", tag, "in", tag.parent
                continue
            for r in reversed(tag.contents):
                tag.parent.insert(i, r)
            tag.extract()
    return soup.renderContents()

def remove_non_ascii(str2):
    strPattern = "[^A-Za-z 0-9 \.,\?'\"!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]"
    strReplace = ""
    return  re.sub(strPattern,strReplace,str2)


class Ebay:
    def __init__(self,user,server):
        self.server = server
        self.user = user
        self.base_url = "http://stores.ebay.in/"
        self.aution = "http://www.ebay.in/sch/i.html?LH_Auction=1&_ipg=200&_ssn=%s"
        self.buy   = "http://www.ebay.in/sch/i.html?LH_BIN=1&_ipg=200&_ssn=%s"
        pass
    
    
    def get_page_link(self,page,number):
        main_page = BeautifulSoup(remove_non_ascii(urlopen(self.server,page)))

        pagenumber =  main_page.find('span',attrs={"class":"page"})
        if pagenumber==None:
            return {}
        paginator = re.findall('[0-9]+',pagenumber.renderContents())
        if len(paginator)!=2:
            return {}
        total_pages = int(paginator[1])
        page_link_base = page + "&_pgn=%i"
        items = {}
        for i in range(total_pages):
            item_page = urlopen(self.server,page_link_base%(i+1))
            for class_link in re.findall("http:\/\/www.ebay.in\/itm\/[^\"\'<> \]\[]*",item_page):
                items[class_link]=number
        return items
    # get all link to item
    def get_all_links(self):
        items =  self.get_page_link(self.aution%self.user,1)
        items2 = self.get_page_link(self.buy%self.user,2)
        for i in items2:
            items[i] = 2
        
        return items
    
    #scrap a item
    def scrap_item(self,link):
        item = {}
        page = BeautifulSoup(remove_non_ascii(urlopen(self.server,link)))
        title = page.find("h1",attrs={"class":"vi-is1-titleH1"})
        title = title.renderContents()
        description = page.find("div",attrs={"class":"item_description"})
        checker = description.find("div",attrs={"id":"ngvi_desc_div"})
	if checker==None:
	    return None
	description = description.renderContents()
        price = page.find("span",attrs={"class":"vi-is1-prcp"}).renderContents().replace(",","").replace(" ","")
        price = float(re.findall("[0-9][0-9\.]*",price)[0])
        try:
            quantity = page.find("span",attrs={"class":"vi-is1-mqtyDiv"}).renderContents()
            quantity  = int(re.findall("[0-9]+",quantity)[0])
        except:
            quantity = 0
        condition = page.find("span",attrs={"class":"vi-is1-condText"}).renderContents()
        z_b = remove_tags(page.find("div",attrs={"class":"z_b"})).replace(" ","")
        item_number = re.findall("Itemnumber:([0-9]*)",z_b)[0]
        location  = re.findall("location:(.*India)S",z_b)[0]
        ships_to = "India" 
        location_helper = location.split(",")
        city = location_helper[0]
        state = location_helper[1]
        pm = page.find("div",attrs={"id":"payDet1"}).renderContents()
        pm = re.findall("\((.*)\)",pm)[0]
        policy = remove_tags(page.find("td",attrs={"class":"vi-rpd-miyContent"})).replace("| Read return policy details","")
        images = re.findall("http:\/\/i\.ebayimg\.com[^| \"\.]+\.JPG",str(page))
        new_images = {}
        for img_352 in images:
            new_images[img_352.replace("14","12")] = 1
        images = []
        for img_500 in new_images:
            images.append(img_500)
        
        """try:
            time_left = remove_tags(page.find("span",attrs={"class":"vi-is1-dt"}))
            finish = re.findall("\((.*)\)",time_left)[0].replace("IST","").strip()
        except:
            finish = ""
        """
        cat = []
        for li in  page.find("ul",attrs={"class":"in"}).findAll("li"):
            cat.append(remove_tags(li))
        cat.reverse()
        
        item["cat"] = cat
        item["description"] = description
        item["title"] = title
        item["price"] = price
        item["quantity"] = quantity
        item["pm"] = pm
        item["policy"] = policy
        item["images"] = images
        item["city"] = city
        item["state"] = state
        return item
         

