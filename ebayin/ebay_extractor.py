'''
Created on 29/03/2012


@author: cristian criscalovis@gmail.com

'''
import os,sys

axd_axd = os.path.realpath(__file__).replace("/ebayin/ebay_extractor.py","/")
os.environ['PYTHON_EGG_CACHE'] = axd_axd
sys.path.append(axd_axd+"scraper")
sys.path.append(os.path.dirname(__file__))
import base64,json
from scrapper.core import *
from ebay import Ebay

import hashlib
            
if __name__ == "__main__":
    request = sys.argv[1]
    try: 
        request = json.loads(base64.b64decode(request))
    except:
        exit()
    request["price_increment"] = int(request["price_increment"]);
    ### LOAD CONFIGURATION FILE ####
    server_folder =  axd_axd+"scraper/manager/"  
    
    try:
        cfile = open(server_folder+"cfile.json")
        server  =  json.loads(cfile.read())
        cfile.close()
    except:
        exit()
        
    server["last_proxy_update"] = 0
    server["proxy_rtime"] = int(server["proxy_rtime"])
    
    #ebay_connection = Ebay("itplaza2011",server)
    #jaja = ebay_connection.get_all_links()
    #print len(jaja)
    #exit()
    ###### Load bad_words #######
    bad_words = [] 
    select_key = {}
    select_key["%bad_word"]   = ""
    select_key["%replace"]   = ""
    objs,o = select(server,select_key)
    keyrel = map_rel(server,select_key)
    bw_index = o.index(keyrel["%bad_word"])
    re_index = o.index(keyrel["%replace"])
    for i in  objs:
        bad_words.append({'bad_word':i[bw_index],'replace':i[re_index]})
    
    ebay_user = request["ebay_user"]
    site_user = request["site_user"]
    #####prepare insertion dict####
   
    select_key = {}
    select_key["%user_id"]   = ""
    where_key = {}
    where_key["%user_name"] = site_user
    where_relation = {}
    where_relation["%user_name"] = "="
    objs,o = select(server,select_key,where=(where_key,where_relation))
    
    if len(objs)==0:
        exit()
    user_id = objs[0][0]
    
     
    insert_key =  {}
    insert_key["%auctions_payment_status"] = request.get("payment_status","confirmed")
    insert_key["%auctions_duration"] = int(request["duration"])*24*60
    insert_key["%auctions_ownerid"] = user_id
    insert_key["%auctions_rp"] = request.get("rp","N")
    insert_key["%auctions_country"] = request.get("country","India")
    insert_key["%auctions_ppscint"] = request.get("scint","Y")
    insert_key["%auctions_sc"] = request.get("sc","BP")
    insert_key["%auctions_hpfeat"] = request.get("hpfeat","Y")
    insert_key["%auctions_catfeat"] = request.get("catfeat","N")
    insert_key["%auctions_bolditem"] = request.get("bolditem","Y")
    insert_key["%auctions_hlitem"] = request.get("hlitem","Y")
    insert_key["%auctions_private"] = request.get("private","N")
    insert_key["%auctions_currency"] = request.get("currency","INR")
    insert_key["%auctions_isswap"] = request.get("isswap","N")
    insert_key["%auctions_listin"] = request.get("listin","both")
    insert_key["%auctions_auto_relist"] = request.get("auto_relist","N")
    insert_key["%auctions_auto_relist_bids"] = request.get("auto_relist_bids","N")
    insert_key["%auctions_endtime_type"] = request.get("endtime_type","lts")
    insert_key["%auctions_approved"] = request.get("approved","1")
    insert_key["%auctions_listing_type"] = request.get("listing_type","full")
    insert_key["%auctions_is_escrow_item"] = request.get("is_escrow_item","1")
    insert_key["%auctions_catfeatplus"] = request.get("catfeatplus","N")
    insert_key["%auctions_hpfeatplus"] = request.get("hpfeatplus","N")
    insert_key["%auctions_enlpicitem"] = request.get("enlpicitem","Y")
    insert_key["%auctions_combine_shipp_ww"] = request.get("combine_shipp_ww","T")
    insert_key["%auctions_chacha"] = request.get("chacha","N")
    insert_key["%auctions_combine_shipp"] = request.get("combine_shipp","T")
    insert_key["%auctions_template"] = request.get("template","4")
    insert_key["%auctions_isverify"] = request.get("isverify","1")
    insert_key["%auctions_counter_style"] = request.get("counter_style","7")
    insert_key["%auctions_auto_relist_nb"] = request.get("auto_relist_nb","1")
    insert_key["%auctions_active"] = request.get("active","1")
    insert_key["%auctions_category"] = 0
    insert_key["%auctions_main_category"] = 0	     
    	
    #extract data
    ebay_connection = Ebay(ebay_user,server)
    #links = []
    #links.append("http://www.ebay.in/itm/Beautiful-Skirt-casual-use-Sportswear-100-W-Artengo-Unmatched-quality-/130670614212?pt=IN_Women_s_Apparel&hash=item1e6c9356c4#ht_2034wt_907")
    #exit()
    
    items = ebay_connection.get_all_links()
    print "len: ",len(items)  
    links = []
    for i in  items:
        links.append(i)
    dicto2  = []
    dicto2.append("")
    dicto2.append("standard")
    dicto2.append("dutch")
    duration =  insert_key["%auctions_duration"]*60
    iteration = 0
    index_of_link = 0
    while iteration<len(links)*10:
        iteration = iteration + 1
        if len(links)==0:
            break
        link = links[index_of_link]
        insert_key["%auctions_auctiontype"] = dicto2[items[link]]
        
        try:
            item  = ebay_connection.scrap_item(link)
            insert_key["%auctions_startdate"] = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())
            insert_key["%auctions_enddate"] = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(time.mktime(time.localtime()) +duration))      
            
            #badwords
    	    import math
            price_352 = item["price"]*(1+int(request["price_increment"])/100.0)
    	    price_base = price_352
    	    price_2 = price_base
    	    for wsj in range(19):
    	        index_r  = "r"+str(wsj+1)
    	        index_e  = "e"+str(wsj+1)
    		if not index_e in request:
    		    continue
    		if not index_r in request:
    		    continue
    		val_r = float(request[index_r])
    		val_e = float(request[index_e])
    		if(price_base>val_r):
    		    price_2=math.ceil(price_base*1.0/val_e)*val_e
            insert_key["%auctions_bid_start"] = price_2
            insert_key["%auctions_bid_original"] = price_2
            if items[link] == 2:
                insert_key["%auctions_bn"] = "Y"
                insert_key["%auctions_bn_value"] = price_2
            else:
                insert_key["%auctions_bn"] = "N"
                
            request["below_description"] = request.get("below_description","")
    	    item["description"] = (item["description"]+"<br>"+request["below_description"]).replace("&","&amp;").replace('"',"&quot;").replace("<","&lt;").replace(">","&gt;").replace("'","&apos;")
            item["title"] = item["title"].replace('"',"&quot;").replace("&","&amp;").replace("<","&lt;").replace(">","&gt;").replace("'","&apos;")
            for i in bad_words:
                item["title"] = item["title"].replace(i['bad_word'],i['replace'])
                item["description"] = item["description"].replace(i['bad_word'],i['replace'])
                if i['replace']=='%delete':
                    if i['bad_word'] in item["title"] or i['bad_word'] in item["description"]:
                        continue
        
            insert_key["%auctions_itemname"] = item["title"]
            insert_key["%auctions_description"] = item["description"]
            insert_key["%auctions_keywords"] = item["title"] + item["description"]
            insert_key["%auctions_pm"] = item["pm"]
            insert_key["%auctions_quantity"] = item["quantity"]  
            insert_key["%auctions_return_policy"] = item["policy"]
            
            cat = item["cat"]
            cont = 0
            search = cat[cont].replace("&gt;","").replace("'","&#039;").strip()
            select_key = {}
            select_key["%auctions_categories_id"]   = ""
            select_key["%auctions_categories_parent"]   = ""
            where_key = {}
            where_key["%auctions_categories_name"] = search
            where_relation = {}
            where_relation["%auctions_categories_name"] = "="
            objs,o = select(server,select_key,where=(where_key,where_relation))
            if len(objs)==0:
                search2 = cat[cont].replace("&amp;","&")
                where_key["%auctions_categories_name"] = search2
                objs,o = select(server,select_key,where=(where_key,where_relation))
            if len(objs)!=0:
            	keyrel = map_rel(server,select_key)
            	id = o.index(keyrel["%auctions_categories_id"])
            	parent = o.index(keyrel["%auctions_categories_parent"])
            	item["category"] = objs[0][id]
            	while objs[0][parent]!=0:
                	cont = cont  + 1;
                	select_key = {}
                	select_key["%auctions_categories_id"]       = ""
                	select_key["%auctions_categories_name"]     = ""
                	select_key["%auctions_categories_parent"]   = ""
                	where_key = {}
                	where_key["%auctions_categories_id"] = objs[0][parent]
                	where_relation = {}
                	where_relation["%auctions_categories_id"] = "="
                	objs,o = select(server,select_key,where=(where_key,where_relation))
                	keyrel = map_rel(server,select_key)
                	id = o.index(keyrel["%auctions_categories_id"])
                	parent = o.index(keyrel["%auctions_categories_parent"])
                	name = o.index(keyrel["%auctions_categories_name"])
                	if len(objs)==0:
                    		item["super_category"] = item["category"]
                    		break
                	search = cat[cont].replace("&gt;","").replace("'","&#039;").strip()
                	search2 = cat[cont].replace("&amp;","&")
                	if(objs[0][name]!=search and objs[0][name]!=search2):
                    		item["super_category"] = item["category"]
                    		break
            		try:        
                		item["super_category"] = objs[0][id]
           	 	except:
                		pass
            try:
            	insert_key["%auctions_category"] = item["category"]
            	insert_key["%auctions_main_category"] = item["super_category"]
            except:
		pass
	    insert_key["%auctions_itemCity"] = item["city"]
            insert_key["%auctions_itemState"] = item["state"]
            item["hash"] = link
            m = hashlib.md5()
            m.update(item["hash"])
            insert_key["%auctions_hash"]= m.hexdigest()
            select_key = {}
            select_key["%auctions_id"]       = ""
            where_key = {}
            where_key["%auctions_hash"] = insert_key["%auctions_hash"]
            where_relation = {}
            where_relation["%auctions_hash"] = "="
            objs,o = select(server,select_key,where=(where_key,where_relation))
            if len(objs)>0:
                links.remove(link)
                continue
            insert(server,insert_key)
            select_key = {}
            select_key["%auctions_id"] = ""
            where_key = {}
            where_key["%auctions_hash"] = insert_key["%auctions_hash"]
            where_relation = {}
            where_relation["%auctions_hash"] = "="
            objs,o = select(server,select_key,where=(where_key,where_relation))
            if len(objs)==0:
                links.remove(link)
                continue
            auctions_id  = objs[0][0]
            #img
            index = 0
            newphotos = []
            for i in item["images"]:
                format = re.findall("\.[a-zA-Z]+$",i)
                if len(format)<1:
                    continue
                format = format[0]
                hard = server["img_path"]
                cloud = server["img_name"]
                img_name = str(insert_key["%auctions_category"])+"_"+str(auctions_id)+"_"+ str(time.mktime(time.localtime())).replace(".","")+"_"+str(index) +format
                try:
                    img = urlopen(server,i)
                except:
                    continue
                file_img = open(hard+img_name,"wb");
                file_img.write(img)
                file_img.close()
                newphotos.append(cloud+img_name)
                index = index +1
            photos = newphotos
            
            if len(photos)>0:
                update_key  = {}
                update_key["%auctions_ppimg"] = photos.pop(0)
                where_keys = {}
                where_keys["%auctions_id"] = auctions_id
                where_relations = {}
                where_relations["%auctions_id"] = "="
                update(server,update_key,where=(where_keys,where_relations))
                
            for j in photos:
                insert_key2  = {}
                insert_key2["%auctions_img_path"] = j
                insert_key2["%auctions_img_auction_id"] = auctions_id
                insert(server,insert_key2)
            
            links.remove(link)
        except:
            pass
        
        index_of_link = index_of_link + 1
        if(index_of_link>=len(links)):
            index_of_link = 0
            
        
