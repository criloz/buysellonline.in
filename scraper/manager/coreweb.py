'''
Created on 15/03/2012

@author: root
'''
import os
axd_axd = os.path.realpath(__file__).replace("/scraper/manager/coreweb.py","")
os.environ['PYTHON_EGG_CACHE'] = axd_axd
import json
import urllib,re,time,hashlib,MySQLdb
import random,sys
from scrapper.sites import Olx,ClickIn,ClickIndia,Quikr

def save(server,folder):
    cfile = open(folder+"cfile.json","w")
    cfile.write(json.dumps(server))
    cfile.close()
    
def default(folder):
    try:
        cfile = open(folder + "default.json","r")
        d =  json.load(cfile)
        cfile.close()
        return d
    except:
        return {"pasreturnsword":"default"}
    
def save_default(var,folder):
    text = json.dumps(var)
    cfile = open(folder + "default.json","w")
    cfile.write(text)
    cfile.close()
    
def delete(folder):
    import os
    os.remove(folder + "cfile.json")
    
def header(page,folder):
    page["html"] = page["html"]  + "<html>"
    page["html"] = page["html"]  + "<head>"
    cssfile = open(folder +"style.css")
    css = cssfile.read()
    cssfile.close()
    page["html"] = page["html"]  + '<style>%s</style>'%(css)
    page["html"] = page["html"]  + '<script src="%s"></script>'%("https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js")
    page["html"] = page["html"]  + '<script src="%s"></script>'%("http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js")
    page["html"] = page["html"]  + '<LINK REL="StyleSheet" HREF="%s" TYPE="text/css">'%("http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/base/jquery-ui.css")
    page["html"] = page["html"]  + "</head>"
    page["html"] = page["html"]  + "<body>"
    page["html"] = page["html"]  + "<div class='menu'><a href='index.py?p=main'>Main</a><a href='index.py?p=map'>Mapper</a><a href='index.py?p=link'>Link generator</a></div>"
    page["html"] = page["html"]  + '<div class="content">'

def footer(page):
    page["html"] = page["html"]  + "</div>"
    page["html"] = page["html"]  + "</body>"
    page["html"] = page["html"]  + "</html>"



def map_rel(server,keys):
    key2 = {}
    if "databases" in server:
        for db_name in server["databases"]:
            db = server["databases"][db_name]
            if "tables" in db:
                for tb_name in db["tables"]:
                    tb = db["tables"][tb_name]
                    if "fields" in tb:
                        for fd_name in tb["fields"]:
                            fd = tb["fields"][fd_name]
                            for key in keys:
                                if key in fd:
                                    key2[key] = fd_name
                    
    return key2
    
def map_dbs(server,keys):
    db_for_insertions = {}
    if "databases" in server:
        for db_name in server["databases"]:
            db = server["databases"][db_name]
            if "tables" in db:
                for tb_name in db["tables"]:
                    tb = db["tables"][tb_name]
                    if "fields" in tb:
                        for fd_name in tb["fields"]:
                            fd = tb["fields"][fd_name]
                            for key in keys:
                                if key in fd:
                                    if not db_name in  db_for_insertions:
                                        db_for_insertions[db_name] = {}
                                    if not tb_name in db_for_insertions[db_name]:
                                        db_for_insertions[db_name][tb_name] = {}
                                    if not fd_name in db_for_insertions[db_name][tb_name]:
                                        db_for_insertions[db_name][tb_name][fd_name] = fd
                                    db_for_insertions[db_name][tb_name][fd_name] = db_for_insertions[db_name][tb_name][fd_name].replace(str(key),str(keys[key]))
                    
    return db_for_insertions


def map_where(server,keys,ralations):
    key_names = map_dbs(server,keys)
    relation2 = map_dbs(server,ralations)
    where = ""
    cont = 0
    for dbname in key_names:
        for  tbname in key_names[dbname]:
            for field in key_names[dbname][tbname]:
                if cont == 0 :
                    cont = cont +1 
                    where = " where %s%s'%s'"%(field,relation2[dbname][tbname][field],key_names[dbname][tbname][field])
                    continue
                where = where + " and " + "%s%s'%s'"%(field,relation2[dbname][tbname][field],key_names[dbname][tbname][field])
            break
        break
    return where
    
def insert(server,keys):
    insertion = map_dbs(server,keys)
    for dbname in insertion:
        db = server["databases"][dbname]
        conn = MySQLdb.connect (host = db["host"],user = db["user"], passwd = db["pass"], db = dbname)
        conn.autocommit(True)
        for  tbname in insertion[dbname]:
            values = "values("
            fields = "("
            con = 0
            for field in insertion[dbname][tbname]:
                if con==0:
                    fields = fields + field
                    values = values + "'" +insertion[dbname][tbname][field] + "'"
                    con = con +1
                    continue
                fields = fields + "," + field
                values = values + "," +  "'" +insertion[dbname][tbname][field]+ "'" 
            values = values +")"
            fields = fields + ")"
            sql = "insert into %s %s %s"%(tbname,fields,values)
            #print sql
            conn.cursor().execute(sql)
            break
        conn.close()
        break

def select(server,keys,**mysql):
    insertion = map_dbs(server,keys)
    ret  = ()
    orderL = []
    where = ""
    if "where" in mysql:
        where = map_where(server,mysql["where"][0],mysql["where"][1])

    for dbname in insertion:
        db = server["databases"][dbname]
        conn = MySQLdb.connect (host = db["host"],user = db["user"], passwd = db["pass"], db = dbname)
        for  tbname in insertion[dbname]:
            select = ""
            con = 0
            for field in insertion[dbname][tbname]:
                orderL.append(field)
                if con==0:
                    select = field
                    con = con +1
                    continue
                select  = select + "," + field
            sql = "select %s from %s%s"%(select,tbname,where)
            #print sql
            cursor = conn.cursor()
            cursor.execute(sql)
            break
        ret = cursor.fetchall() 
        conn.close()
        break
    return ret,orderL

def update(server,keys,**mysql):
    insertion = map_dbs(server,keys)
    where = ""
    if "where" in mysql:
        where = map_where(server,mysql["where"][0],mysql["where"][1])

    for dbname in insertion:
        db = server["databases"][dbname]
        conn = MySQLdb.connect (host = db["host"],user = db["user"], passwd = db["pass"], db = dbname)
        conn.autocommit(True)
        for  tbname in insertion[dbname]:
            set2 = ""
            con = 0
            for field in insertion[dbname][tbname]:
                if con==0:
                    set2 = "set %s='%s'"%(field,insertion[dbname][tbname][field])
                    con = con +1
                    continue
                set2  = set2 + "," + "%s='%s'"%(field,insertion[dbname][tbname][field])
            sql = "update %s %s%s"%(tbname,set2,where)
            #print sql
            cursor = conn.cursor()
            cursor.execute(sql)
            break
        conn.close()
        break
    
class Category():
    def __init__(self,title,link):
        self.link = link
        self.title = title
        self.only_with_images = True
        self.categories = []
        self.max_time = 60*60*24*30
        pass
    def add_category(self,title,link):
        self.categories.append(Category(title,link))

