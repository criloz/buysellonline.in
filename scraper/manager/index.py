#!/usr/bin/python
'''
Created on 14/03/2012

@author: cristian criscalovis@gmail.com
'''
import os,sys
axd_axd = os.path.realpath(__file__).replace("/manager/index.py","/")
sys.path.append(axd_axd)

from manager.coreweb import *
from manager.login_lib import check_login
from manager.mainpage import main
from manager.mapper import mapper
from manager.linker import linker
from manager.login import login_page
from scrapper.BeautifulSoup import BeautifulSoup 
import Cookie
import cgi


def application(environ,start_response):
  
    import os
    main_folder_xxx = os.path.realpath(__file__).replace("/index.py","/")
    
    server = {}
    try:
        cfile = open(main_folder_xxx+"cfile.json")
        server  =  json.loads(cfile.read())
        cfile.close()
    except:
        server["password"] = "default"
    
    page = {}
    page["html"] = ""
    
    user = {}
    user["login"] = False
    
    cookie = Cookie.SimpleCookie()
    cookie.load(environ.get("HTTP_COOKIE",""))
    
    if cookie.has_key('session'):
        user["session"] = cookie["session"].value
      
    
    sites = []
    sites.append(Olx())
    sites.append(ClickIn())
    sites.append(ClickIndia())
    sites.append(Quikr())
    
    user["ip"] = environ.get("REMOTE_ADDR",'UNKNOWN')
    
    form = cgi.FieldStorage(fp=environ['wsgi.input'], environ=environ)
    
    page_xxx = None
      
    check_login(user,server)
    
    header(page,main_folder_xxx)
    in_login = False;
    if not user["login"]:
        cookie = Cookie.SimpleCookie()
        login_page(form,cookie,server,page,user,main_folder_xxx)
        in_login = True;
    else:
        page_xxx = form.getvalue("p", None)
        if page_xxx=="map":
            mapper(form,page,server,main_folder_xxx,sites)
        elif page_xxx=="link":
            linker(page,server)
        else:
            main(form,page,server,main_folder_xxx)
    
    footer(page)
    status = '200 OK'
    output = page["html"].encode('utf-8') 
    response_headers = [('Content-type', 'text/html; charset=utf-8'),
                        ('Content-Length', str(len(output)))]
    if in_login:
        response_headers.append(('Set-Cookie',str(cookie).replace("Set-Cookie:","").strip()))
    start_response(status, response_headers)

    return [output]

    






