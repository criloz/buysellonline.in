#!/usr/bin/python
from coreweb import *

def load_categories(page,server):
    select_key = {}
    select_key["%sites_category_map"] = ""
    select_key["%sites_category_site"] = ""
    where_keys = {}
    where_keys["%sites_category_map"] = "0"
    where_relations = {}
    where_relations["%sites_category_map"] = "!="
    objs,o = select(server,select_key,where=(where_keys,where_relations))
    keyrel = map_rel(server,select_key)
    map       =    o.index(keyrel["%sites_category_map"])
    site      =    o.index(keyrel["%sites_category_site"])
    page["html"] =page["html"] + "<script>"
    page["html"] =page["html"] + "var categories = new Array(" 
    cat_site = {}
    sites_js = {}
    for i in  objs:
        sites_js[i[site]]=1
        if i[map] in cat_site:
            cat_site[i[map]]["sites"][i[site]]=1
            continue
        cat_site[i[map]] = {}
        cat_site[i[map]]["sites"] = {}
        cat_site[i[map]]["sites"][i[site]] = 1
        select_key = {}
        select_key["%host_category_name_read"] = ""
        select_key["%host_category_parent_read"] = ""
        where_keys = {}
        where_keys["%host_category_id_read"] = i[map]
        where_relations = {}
        where_relations["%host_category_id_read"] = "="
        query,order = select(server,select_key,where=(where_keys,where_relations))
        keyrel = map_rel(server,select_key)
        host_name       =    order.index(keyrel["%host_category_name_read"])
        host_parent     =    order.index(keyrel["%host_category_parent_read"])
        if len(query)==0:
            continue
        subcat_name   =   query[0][host_name]
        subcat_parent =   query[0][host_parent]
        select_key = {}
        select_key["%host_category_name_read"] = ""
        where_keys = {}
        where_keys["%host_category_id_read"] = subcat_parent
        where_relations = {}
        where_relations["%host_category_id_read"] = "="
        query,order = select(server,select_key,where=(where_keys,where_relations))
        if len(query)==0:
            continue
        cat_name = query[0][0]
        value = '"%s @category:%s"'%(subcat_name,cat_name)
        cat_site[i[map]]["in_db"] = {"category":cat_name,"sub_category":subcat_name}
        cat_site[i[map]]["value"] = value
    js_dict = {}
    great_categories = {}
    great_categories2 = {}
    translator = {}
    ids = []
    for i in cat_site:
        translator[i]  = cat_site[i]["value"].replace('"',"")
        ids.append(i)
        js_dict[cat_site[i]["value"].replace('"',"")] = {"sites":cat_site[i]["sites"],"id":i}
        if not cat_site[i]["in_db"]["category"] in great_categories:
            great_categories[cat_site[i]["in_db"]["category"]] = {}
            great_categories[cat_site[i]["in_db"]["category"]]["sites"] = {}
            great_categories[cat_site[i]["in_db"]["category"]]["ids"] = []
            for www in cat_site[i]["sites"]:
                great_categories[cat_site[i]["in_db"]["category"]]["sites"][www]=1
            great_categories[cat_site[i]["in_db"]["category"]]["ids"].append(i)
        else:
            great_categories2[cat_site[i]["in_db"]["category"]] = great_categories[cat_site[i]["in_db"]["category"]]
            for j in cat_site[i]["sites"]:
                great_categories2[cat_site[i]["in_db"]["category"]]["sites"][j] = 1
            great_categories2[cat_site[i]["in_db"]["category"]]["ids"].append(i)
    cont = 0;     
    for i in js_dict:
        if cont == 0:
            page["html"] =page["html"] + '"%s"'%i
            cont = cont +1
            continue
        page["html"] =page["html"] + "," + '"%s"'%i
    page["html"] =page["html"] + ");"
    help = []
    for xxx in sites_js:
        help.append(xxx)
    sites_js = help
    page["html"] =page["html"] + 'var main_dict=%s;'%json.dumps(js_dict)
    page["html"] =page["html"] + 'var super_cat =%s;'%json.dumps(great_categories2)
    page["html"] =page["html"] + 'var sites =%s;'%json.dumps(sites_js)
    page["html"] =page["html"] + 'var id_translator =%s;'%json.dumps(translator)
    page["html"] =page["html"] + 'var main_url = "%s";'%server["main_url"]
    page["html"] =page["html"] + 'var behav = [1,0];'
    page["html"] =page["html"] + "</script>" 
    page["html"] =page["html"] + '<div class="section">'
    page["html"] =page["html"] + '<span class="title">Link Decoder</span>'
    page["html"] =page["html"] + '<textarea id="link_decode" style="width:90%;height:100px; margin:auto"></textarea><br>'
    page["html"] =page["html"] + '<input type="button" onclick="decode()" value="Decode Link"/>'
    page["html"] =page["html"] + '<input type="button" onclick ="delete_all()" value="Delete All"/>'
    page["html"] =page["html"] + '</div">'
    
    page["html"] =page["html"] + '<div class="section">'
    page["html"] =page["html"] + '<span class="title">Add super Categories</span>'
    index = 0
    great_categories2["All"] = {}
    great_categories2["All"]["ids"] = ids
    for i in great_categories2:
        page["html"] =page["html"] + '<div class="sub_section">'
        page["html"] =page["html"] + '<span class="title">%s</span>'%i
        page["html"] =page["html"] + '<table><thead><tr><th>Only with images</th><th>adds number</th><th></th></tr></thead><tbody><tr>'
        page["html"] =page["html"] + '<td style="text-align:center;"><input id="supert_cat_img_%i" type="checkbox"/></td>'%index
        page["html"] =page["html"] + '<td style="text-align:center;"><input id="supert_cat_num_%i" type="text" style="width:36px;"/></td>'%index
        page["html"] =page["html"] + "<td style='text-align:center;'><input type='button' id='add_button' value='Add' onclick='var temp1=jQuery(\"#supert_cat_img_%i\").attr(\"checked\")==undefined?0:1;var temp2=parseInt(jQuery(\"#supert_cat_num_%i\").val());temp2=isNaN(temp2)?0:temp2;"%(index,index)
        for j in great_categories2[i]["ids"]:
            page["html"] =page["html"] + 'add_category(id_translator[\"%s\"],{\"0\":[temp1,temp2]});'%(j)
        page["html"] =page["html"] + "drawAll();jQuery(\"#supert_cat_num_%i\").val(\"\");jQuery(\"#supert_cat_img_%i\").removeAttr(\"checked\");change_link()'/></td>"%(index,index)
        page["html"] =page["html"] + '</tr></tbody></table>'
        page["html"] =page["html"] + "</div>"
        index = index + 1
        
    page["html"] =page["html"] + '</div>'
    
def js_functions(page):
    page["html"] =page["html"] + "<script>"
    #{"0":[img,number],"1":[img,number],"2":[img,number],"3":[img,number],"5":[img,number]]
    page["html"] =page["html"] + """
        var main_query = {};
        var show = {};
        function add_category(name,value){
            if(main_dict[name]!=undefined){
                main_query[main_dict[name]["id"]] = value;
            }
        }
        function delete_category(name){
            delete main_query[main_dict[name]["id"]];
            
        }
        function decode(){
            link_2 = jQuery("#link_decode").val();
            link_2 = JSON.parse(atob(link_2.replace(main_url+"?request=","")));
            sites  = link_2["sites"];
            main_query = link_2["categories"];
            behav  = link_2["behav"];
            jQuery("#exp_time").val(link_2["ads_duration"])
            drawAll();
            
        }
        function delete_all(){
            main_query = {};
            removeAll();
            change_link();
        }
        function change_link(){
            link = main_url;
            obj = {}
            obj["sites"] = sites;
            obj["categories"] = main_query;
            obj["behav"] = behav;
            obj["ads_duration"] = parseInt(jQuery("#exp_time").val());
            obj["ads_duration"] = isNaN(obj["ads_duration"])?0:obj["ads_duration"];
            
            jQuery("#link").html(main_url+"?request="+btoa(JSON.stringify(obj)));
        }
        function drawAll(){
            removeAll();
            for(key in main_query){
                draw(key);
            }
            change_link(); 
        }
        function removeAll(){
            jQuery("#cat_panel").empty();
            show = {};
        }
        
        function draw(key){
        if(behav[0]==1){
            if(key != undefined && show[key]==undefined){
                sub     =  jQuery(jQuery('<div id="'+key+'" class="sub_section"></div>'));
                title   =  jQuery(jQuery('<span class="title">'+id_translator[key]+' </span>'));
                delete_obj = jQuery(jQuery('<a href="#">X</a>'));
                title.append(delete_obj);
                delete_obj.click({key_id:key},function(event){
                        key = event.data.key_id;
                        jQuery('#'+key).remove();
                        delete show[key];
                        delete main_query[key];
                        change_link();
                    });
                tab     =  jQuery(jQuery('<table></table>'));
                sub.append(title);
                sub.append(tab);
                thead   =  jQuery(jQuery("<thead></thead>"));
                tab.append(thead)
                tbody   =  jQuery(jQuery("<tbody></tbody>"));
                tab.append(tbody);
                titles  =  jQuery(jQuery("<tr></tr>"));
                values  =  jQuery(jQuery("<tr></tr>"));
                thead.append(titles)
                tbody.append(values)
                
                tb_2   = jQuery(jQuery("<table></table>"));
                th_2 = jQuery(jQuery("<thead>></thead>"));
                tbo_2 = jQuery(jQuery("<tbody></tbody>"));
                tb_2.append(th_2);
                tb_2.append(tbo_2);
                global = jQuery(jQuery("<th>Global</th>"));
                titles.append(global);
                globalval = jQuery(jQuery("<td></td>"));
                values.append(globalval);
                globalval.append(tb_2)
                tit_2 = jQuery(jQuery("<tr></tr>"));
                val2 = jQuery(jQuery("<tr></tr>"));
                th_2.append(tit_2)
                tbo_2.append(val2)
                
                owi = jQuery(jQuery("<th>Only with images</th>"));
                owival = jQuery(jQuery("<td></td>"));
                owicheck = jQuery(jQuery('<input type="checkbox" id="img_'+key+'">'));
                if(main_query[key]["0"][0]==1){owicheck.attr("checked","true")};
                tit_2.append(owi)
                val2.append(owival)
                owival.append(owicheck)
                owicheck.change({key_id:key},function(event){
                    key = event.data.key_id;
                    obj = jQuery(this);
                    if(obj.attr("checked")!=undefined){main_query[key]["0"][0] = 1;}
                    else{main_query[key]["0"][0] = 0;}
                    change_link();
                })
                number = jQuery(jQuery("<th>adds number</th>"));
                number_val  = jQuery(jQuery("<td></td>"));
                number_input = jQuery(jQuery('<input type="text" id="num_'+key+'" value="'+main_query[key]["0"][1]+'">'));
                number_input.change({key_id:key},function(event){
                    key = event.data.key_id;
                    obj = jQuery(this);
                    main_query[key]["0"][1] = parseInt(obj.val());
                    main_query[key]["0"][1]  = isNaN(main_query[key]["0"][1])?0:main_query[key]["0"][1];
                    change_link();
                })
                number_val.append(number_input)
                val2.append(number_val)
                tit_2.append(number)
                index = 0;
                for(site in sites){
                    index = index +1;
                    if(!(sites[site] in main_dict[id_translator[key]]["sites"])){
                        continue;
                    }
                    site = jQuery(jQuery("<th>"+sites[site]+"</th>"));
                    owicheck = jQuery(jQuery('<input type="checkbox" id="site_'+key+'_'+index+'">'));
                    owicheck.change({key_id:key,indx:index},function(event){
                        key = event.data.key_id;
                        index = event.data.indx;
                        obj = jQuery(this);
                        if(obj.attr("checked")==undefined){delete main_query[key][""+index];}
                        else{
                                img_val = 0;
                                if(jQuery('#img_'+key+'_'+index).attr("checked")!=undefined){img_val = 1;};
                                num_val = parseInt(jQuery('#num_'+key+'_'+index).val());
                                num_val = isNaN(num_val)?0:num_val;
                                main_query[key][""+index] = [img_val,num_val];
                            }
                        change_link();
                    })
                    if(main_query[key][""+index]!=undefined){
                        owicheck.attr("checked","true");
                    };
                    site.append(owicheck)
                    titles.append(site);
                    siteval = jQuery(jQuery("<td></td>"));
                    values.append(siteval)
                    tb_2   = jQuery(jQuery("<table></table>"));
                    siteval.append(tb_2);
                    th_2 = jQuery(jQuery("<thead>></thead>"));
                    tbo_2 = jQuery(jQuery("<tbody></tbody>"));
                    tb_2.append(th_2);
                    tb_2.append(tbo_2);
                    
                    tit_2 = jQuery(jQuery("<tr></tr>"));
                    val2 = jQuery(jQuery("<tr></tr>"));
                    th_2.append(tit_2)
                    tbo_2.append(val2)
                    
                    owi = jQuery(jQuery("<th>Only with images</th>"));
                    owival = jQuery(jQuery("<td></td>"));
                    owicheck = jQuery(jQuery('<input type="checkbox" id="img_'+key+'_'+index+'">'));
                    if(main_query[key][""+index]!=undefined){
                        if(main_query[key][""+index][0]==1){owicheck.attr("checked","true")};
                    }
                    tit_2.append(owi)
                    val2.append(owival)
                    owival.append(owicheck)
                    owicheck.change({key_id:key,indx:index},function(event){
                        key = event.data.key_id;
                        index = event.data.indx;
                        obj = jQuery(this);
                        if(main_query[key][""+index]!=undefined){
                            if(obj.attr("checked")!=undefined){main_query[key][""+index][0] = 1;}
                            else{main_query[key][""+index][0] = 0;}
                            change_link();
                        }
                    })
                    number = jQuery(jQuery("<th>adds number</th>"));
                    number_val  = jQuery(jQuery("<td></td>"));
                    number_val_number = main_query[key][""+index]==undefined?0:main_query[key][""+index][1];
                    number_input = jQuery(jQuery('<input type="text" id="num_'+key+'_'+index+'" value="'+number_val_number+'">'));
                    number_input.change({key_id:key,indx:index},function(event){
                        key = event.data.key_id;
                        index = event.data.indx;
                        obj = jQuery(this);
                        if(main_query[key][""+index]!=undefined){
                            main_query[key][""+index][1] = parseInt(obj.val());
                            main_query[key][""+index][1] = isNaN(main_query[key][""+index][1])?0:main_query[key][""+index][1];
                            change_link();
                        }
                    })
                    number_val.append(number_input)
                    val2.append(number_val)
                    tit_2.append(number)
                    
                }
                jQuery("#cat_panel").prepend(sub);
                show[key] = true;
            }
            }
        else{
                 if(key != undefined && show[key]==undefined){
                      sub     =  jQuery(jQuery('<div id="'+key+'" class="sub_section"></div>'));
                      sub.css("border-color","red");
                      title   =  jQuery(jQuery('<span class="title">'+id_translator[key]+' </span>'));
                      sub.append(title);
                      jQuery("#cat_panel").prepend(sub);
                      show[key] = true;
                      delete_obj = jQuery(jQuery('<a href="#">X</a>'));
                        title.append(delete_obj);
                       delete_obj.click({key_id:key},function(event){
                        key = event.data.key_id;
                        jQuery('#'+key).remove();
                        delete show[key];
                        delete main_query[key];
                        change_link();
                    });
                     
                 }
            }
        }
    """
    page["html"] =page["html"] + "</script>" 

def print_form(page):
    page["html"] =page["html"] + '<div class="section">'
    page["html"] =page["html"] + '<span class="title">Categories Configurator</span>'
    page["html"] =page["html"] + "<input style='width:450px' type='text' id='add_category' />"
    page["html"] =page["html"] + "<input type='button' id='add_button' value='Add Category' onclick='add_category(jQuery(\"#add_category\").val(),{\"0\":[0,0]});draw(main_dict[jQuery(\"#add_category\").val()][\"id\"]);jQuery(\"#add_category\").val(\"\");change_link()' />"
    page["html"] =page["html"] + """<script>
                    jQuery("#add_category").autocomplete({
                             source: categories,
                             minLength:3
                     });
                  </script>"""
    page["html"] =page["html"] + "<br>"
    page["html"] =page["html"] + 'Only <input id="only" type="checkbox" checked/>'
    page["html"] =page["html"] + ' Not <input id="not" type="checkbox"/>'
    page["html"] =page["html"] + """<script>
                    jQuery("#add_category").autocomplete({
                             source: categories,
                             minLength:3
                     });
                     jQuery("#only").change(function(e){
                         obj = jQuery(this);
                         if(obj.attr("checked")==undefined){
                             jQuery("#not").attr("checked",true);
                             behav = [0,1];
                             drawAll();
                         }
                         else{
                             jQuery("#not").removeAttr("checked");
                             behav = [1,0];
                             drawAll();
                         }
                     });
                    jQuery("#not").change(function(e){
                         obj = jQuery(this);
                         if(obj.attr("checked")==undefined){
                             jQuery("#only").attr("checked",true);
                              behav = [1,0];
                              drawAll();
                         }
                         else{
                             jQuery("#only").removeAttr("checked");
                             behav = [0,1];
                             drawAll();
                         }
                     });
                  </script>"""
    page["html"] =page["html"] + '</div>'
    page["html"] =page["html"] + '<div id="cat_panel" class="section">'
    page["html"] =page["html"] + '</div>'
    
    
    page["html"] =page["html"] + '<div class="section">'
    page["html"] =page["html"] + '<span class="title">link</span>'
    page["html"] =page["html"] + '<div id="link">'
    page["html"] =page["html"] + '</div>'
    page["html"] =page["html"] + '</div>'       

def expiration(page):
    page["html"] =page["html"] + '<div class="section">'
    page["html"] =page["html"] + '<span class="title">Ads Expiration time (in minutes)</span>'
    page["html"] =page["html"] + "<input style='width:450px' value='10080' type='text' id='exp_time' onchange='change_link()'/>"
    page["html"] =page["html"] + '</div>'
    
def linker(page,server):
    load_categories(page,server)
    expiration(page)
    print_form(page)
    js_functions(page)
