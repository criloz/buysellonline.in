#!/usr/bin/python

'''
Created on 15/03/2012
'''

from coreweb import *
from login_lib import *

def login_page(form,cookie,server,page,user,folder):
    password = form.getvalue("password",None)
    if password==None:
        login_form(page)
    else:
        login(server,page,user,password,cookie,folder)