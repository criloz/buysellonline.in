from coreweb import *

def generate_key():
    import random
    upper = "ABCDEFGHIJKLMNOPQRSTUWXYZ"
    lower = upper.lower()
    especial = "|!@#.$%&/()=?"
    numbers = "0123456789"
    all = upper+lower+especial+numbers
    random.seed(time.mktime(time.localtime()))
    key = ""
    for i in range(30):
        key = key  + random.choice(all)
    return key

def create_session(cookie,server,user):
    key = generate_key()
    expdate  = time.mktime(time.localtime()) + 60*60*24
    ip = user["ip"]
    server["session"] = [key,expdate,ip]
    cookie['session'] =  key


def check_login(user,server):
    current = time.mktime(time.localtime())
    if "session" in user:
        if "session" in server:
            if current<server["session"][1]:
                if server["session"][0]==user["session"] and server["session"][2]==user["ip"]:
                    user["login"] = True

def logout(server,folder):
    del server["session"]
    save(server,folder)

def login(server,page,user,password,cookie,folder):
    if server["password"]==password:
        create_session(cookie,server,user)
        save(server,folder)
        user["login"] = True
    else:
        page["html"] = page["html"]+ "Bad password"
        
def login_form(page):
    page["html"] = page["html"]+ """
        <form method="POST" action="index.py">
            <label for="password">Password</label>
            <input name = "password" type="password" />
            <input type="submit" />
        </form>
    """
