#!/usr/bin/python
'''
Created on 15/03/2012

@author: root
'''
from coreweb import *
from scrapper.core import Adds
db_name = ""
db_host = ""
db_user  = ""
db_password = ""
tb_name = ""
fd_name = ""
img_path  = ""
img_name = ""
def new_db_form(page):
    page["html"] = page["html"]  +  '<div class="section"><span class="title">New Database</span>'
    page["html"] = page["html"]  +  """
        <form class="new_db" method="GET" action="index.py">
            <input value="main"  name="p" type="hidden"/>
            <input value="add_db"  name="a" type="hidden"/>
            <table><tbody>
            <tr>
                <td><label for="db_name">Database name</label></td>
                <td><input id="db_name" name="db_name" value="%s" type="text"/></td>
            </tr>
            <tr>
                <td><label for="db_host">Database host</label></td>
                <td><input id="db_host" name="db_host" value="%s" type="text"/></td>
            </tr>
            <tr>
                <td><label for="db_user">Database user</label></td>
                <td><input id="db_user" name="db_user" value="%s" type="text"/></td>
            </tr>
            <tr>
                <td><label for="db_pass">Database password</label></td>
                <td><input id="db_pass" name="db_pass" value="%s" type="text"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input value="Add database" type="submit"></td>
            </tr>
            </tbody></table>
        </form>"""%(db_name,db_host,db_user,db_password)
    page["html"] = page["html"]  +  "</div>"
    
def databases(page,server):
    page["html"] = page["html"]  +  '<div class="section"><span class="title">Fields settings</span>'
    if "databases" in server:
        for db in server["databases"]:
            i = server["databases"][db]
            page["html"] = page["html"]  +  '<div class="sub_section"><span class="title">%s <a href="index.py?p=main&a=delete_db&db=%s" title="delete">X</a></span><br>'%(db,db)
            if "tables" in i:
                page["html"] = page["html"]  +  '<ul class="list_of_tables">'
                for tb in i["tables"]:
                    page["html"] = page["html"]  +  '<li class="table_name_element">%s <a href="index.py?p=main&a=delete_table&db=%s&t=%s" title="delete">X</a></li>'%(tb,db,tb) 
                    if "fields" in i["tables"][tb]:
                        page["html"] = page["html"]  +  '<ul class="list_of_fields">'
                        for field in i["tables"][tb]["fields"]:
                            fd =  i["tables"][tb]["fields"][field]
                            page["html"] = page["html"]  +  """<li class="field"><span class="field_name">%s</span>
                                    <form style="display:inline" action="index.py" method="GET">
                                    <input value="%s"  name="db_name" type="hidden"/>
                                    <input value="%s"  name="tb_name" type="hidden"/>
                                    <input value="%s"  name="fd_name" type="hidden"/>
                                    <input value="main"  name="p" type="hidden"/>
                                    <input value="save_fd"  name="a" type="hidden"/>
                                    <input value="%s"  name="fd_value" type="text"/>
                                     <input value="save" type="submit">
                                    </form>
                                    <a href="index.py?p=main&a=delete_fd&db=%s&t=%s&fd=%s" title="delete">X</a></li>"""%(field,db,tb,field,fd,db,tb,field) 
                        page["html"] = page["html"]  +  '</ul>'
                    page["html"] = page["html"]  +  """<form method="GET" action="index.py">
                    <label for="fd_name">field name</label>
                    <input id="fd_name" name="fd_name" value="" type="text"/>
                    <input value="main"  name="p" type="hidden"/>
                    <input value="add_fd"  name="a" type="hidden"/>
                    <input value="%s"  name="db_name" type="hidden"/>
                    <input value="%s"  name="tb_name" type="hidden"/>
                    <input value="Add field" type="submit">
                    </form>"""%(db,tb)
                page["html"] = page["html"]  +  '</ul>'
            page["html"] = page["html"]  +  """<form method="GET" action="index.py">
            <label for="tb_name">table name</label> 
            <input id="tb_name" name="tb_name" value="" type="text"/>
            <input value="main"  name="p" type="hidden"/>
            <input value="add_tb"  name="a" type="hidden"/>
            <input value="%s"  name="db_name" type="hidden"/>
            <input value="Add table" type="submit">
            </form>"""%(db)            
            page["html"] = page["html"]  +  '</div>'
    page["html"] = page["html"]  +  '</div>'

def commands(page):
    testAdds = Adds()
    command_list = testAdds.command_list
    page["html"] = page["html"]  +  '<div class="section">'
    page["html"] = page["html"]  +  '<span class="title">Commands</span>'
    page["html"] = page["html"]  +  '<ul class="command_list">'
    for i in command_list:
        page["html"] = page["html"]  +  '<li>%s</li>'%i
    page["html"] = page["html"]  +  '</ul>'
    page["html"] = page["html"]  +  '</div>'
    

def proxy(server,page):
    pp = ""
    pr = ""
    if "proxy_path" in server:
        pp = server["proxy_path"]
    if "proxy_rtime" in server:
        pr = server["proxy_rtime"]
    page["html"] = page["html"]  +  '<div class="section">'
    page["html"] = page["html"]  +  '<span class="title">Proxy</span>'
    page["html"] = page["html"]  +  """
        <form class="new_db" method="GET" action="index.py">
            <input value="main"  name="p" type="hidden"/>
            <input value="save_proxy"  name="a" type="hidden"/>
            <table><tbody>
            <tr>
                <td><label for="proxy_path">Proxy file</label></td>
                <td><input id="proxy_path" name="proxy_path" value="%s" type="text"/></td>
            </tr>
            <tr>
                <td><label for="proxy_rtime">Rate for reload proxy file</label></td>
                <td><input id="proxy_rtime" name="proxy_rtime" value="%s" type="text"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input value="Save" type="submit"></td>
            </tr>
            </tbody></table>
        </form>"""%(pp,pr)

    page["html"] = page["html"]  +  '</div>'
       

def images(server, page):
    p = ""
    n = ""
    if "img_path" in server:
        p = server["img_path"]
    
    if "img_name" in server:
        n = server["img_name"]
        
    page["html"] = page["html"]  +  '<div class="section">'
    page["html"] = page["html"]  +  '<span class="title">Images Folder</span>'
    page["html"] = page["html"]  +  """
        <form class="new_db" method="GET" action="index.py">
            <input value="main"  name="p" type="hidden"/>
            <input value="save_img"  name="a" type="hidden"/>
            <table><tbody>
            <tr>
                <td><label for="img_path">Folder path</label></td>
                <td><input id="img_path" name="img_path" value="%s" type="text"/></td>
            </tr>
            <tr>
                <td><label for="img_name">Name for photos</label></td>
                <td><input id="img_name" name="img_name" value="%s" type="text"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input value="Save" type="submit"></td>
            </tr>
            </tbody></table>
        </form>"""%(p,n)
    page["html"] = page["html"]  +  '</div>'


def change_password(page):
    page["html"] = page["html"]  +  '<div class="section">'
    page["html"] = page["html"]  +  '<span class="title">Change Password</span>'
    page["html"] = page["html"]  +  """
        <form class="new_db" method="GET" action="index.py">
            <input value="main"  name="p" type="hidden"/>
            <input value="change_pass"  name="a" type="hidden"/>
            <table><tbody>
            <tr>
                <td><label for="c_pass">Current pass</label></td>
                <td><input id="c_pass" name="c_pass" value="" type="password"/></td>
            </tr>
            <tr>
                <td><label for="pass1">New password</label></td>
                <td><input id="pass1" name="pass1" value="" type="password"/></td>
            </tr>
            <tr>
                <td><label for="pass2">Repeat password</label></td>
                <td><input id="pass2" name="pass2" value="" type="password"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input value="Change password" type="submit"></td>
            </tr>
            </tbody></table>
        </form>"""
    page["html"] = page["html"]  +  '</div>'
    pass

def save_form(page):
    page["html"] = page["html"]  +  '<div class="section">'
    page["html"] = page["html"]  +  '<span class="title">Save settings</span>'
    page["html"] = page["html"]  +  """
        <form class="new_db" method="GET" action="index.py">
            <input value="main"  name="p" type="hidden"/>
            <input value="s_d"  name="a" type="hidden"/>
            <input value="Set default" type="submit">
        </form>"""
    page["html"] = page["html"]  +  """
        <form class="new_db" method="GET" action="index.py">
            <input value="main"  name="p" type="hidden"/>
            <input value="r_d"  name="a" type="hidden"/>
            <input value="Restore default" type="submit">
        </form>"""
    page["html"] = page["html"]  +  """
        <form class="new_db" method="GET" action="index.py">
            <input value="main"  name="p" type="hidden"/>
            <input value="del_all"  name="a" type="hidden"/>
            <input value="Delete all" type="submit">
        </form>"""
    page["html"] = page["html"]  +  '</div>'
    pass

def site_link(server,page):
    mu = ""
    try:
        mu = server["main_url"]
    except:
        pass
    page["html"] = page["html"]  +  '<div class="section">'
    page["html"] = page["html"]  +  '<span class="title">Script main link</span>'
    page["html"] = page["html"]  +  """
        <form class="new_db" method="GET" action="index.py">
            <input value="main"  name="p" type="hidden"/>
            <input value="sml"  name="a" type="hidden"/>
            <label for="main_url">Main url </label>
            <input id="main_url" name="main_url" value="%s" type="text"/>
            <input value="Save" type="submit">
        </form>"""%(mu)
    page["html"] = page["html"]  +  '</div>'
    pass


    
def main(form,page,server,folder):
    import os
    global db_name,db_host,db_user,db_password,tb_name,fd_name,img_path,img_name
    action = form.getvalue("a", None)
    if action=="sml":
        server["main_url"]  = form.getvalue("main_url", "")
        save(server,folder)
    if action == "save_proxy":
        path = form.getvalue("proxy_path", "")
        server["proxy_rtime"]  = form.getvalue("proxy_rtime", "")
        if os.path.isfile(path):
            server["proxy_path"] = path
        else:
            page["html"] = page["html"]  +  '<div class="error">the file not exist</div>'
        save(server,folder)
        
    if action=="s_d":
        save_default(server,folder)
        
    if action=="r_d":
        session = None
        if "session" in server:
            session = server["session"]
        server2 = default(folder)
        for i in server2:
            server[i] = server2[i]
        if session!=None:
            server["session"]  = session
        save(server,folder)
        
    if action=="del_all":
        delete_list = []
        for i in server:
            delete_list.append(i)
        for  i in delete_list:
            del server[i]
        delete(folder)
        
    if action=="change_pass":
        c_pass     = form.getvalue("c_pass", "")
        pass1      = form.getvalue("pass1", "")
        pass2      = form.getvalue("pass2", "")     
        if(c_pass==server["password"]):
            if pass1 == pass2:
                if len(pass1)>4:
                    server["password"] = pass1
                    save(server,folder)
                else:
                    page["html"] = page["html"]  +  '<div class="error">the password must have as min 4 characters</div>'
            else:
                page["html"] = page["html"]  +  '<div class="error">the password are not equal</div>'
                
        else:
            page["html"] = page["html"]  +  '<div class="error">bad current password</div>'
                
            
    if action=="save_img":
        img_name     = form.getvalue("img_name", "")
        img_path     = form.getvalue("img_path", "")
        server["img_name"] = img_name
        if os.path.isdir(img_path):
            server["img_path"] = img_path
        else:
            page["html"] = page["html"]  +  '<div class="error">bad path for img folder</div>'
        save(server,folder)
            
    if action=="add_db":
        db_name     = form.getvalue("db_name", "")
        db_host     = form.getvalue("db_host", "")
        db_user     = form.getvalue("db_user", "")
        db_password = form.getvalue("db_pass", "")
        if db_name=="" or db_host=="" or db_user=="" or db_password=="":
            page["html"] = page["html"]  +  '<div class="error">please enter the data  for the db</div>'
        else:
            try:
                conn = MySQLdb.connect (host = db_host,user = db_user, passwd = db_password, db = db_name)
                conn.close()
                if not "databases" in server:
                    server["databases"] = {}
                if not db_name in server["databases"]:
                    server["databases"][db_name] = {"host":db_host,"user":db_user,"pass":db_password}
                    save(server,folder)
                db_name = ""
                db_host = ""
                db_user  = ""
                db_password = ""
            except:
                page["html"] = page["html"]  +  '<div class="error">bad database data</div>'
    if action=="add_tb":
        db_name     = form.getvalue("db_name", "")
        tb_name     = form.getvalue("tb_name", "")
        if tb_name=="":
            page["html"] = page["html"]  +  '<div class="error">bad table name</div>'
        elif "databases" in server:
            if db_name in server["databases"]:
                db = server["databases"][db_name]
                conn = MySQLdb.connect (host = db["host"],user = db["user"], passwd = db["pass"], db = db_name)
                cursor = conn.cursor()
                cursor.execute("show tables")
                tables =  cursor.fetchall()
                conn.close()
                found = False
                for table in tables:
                    if table[0]==tb_name:
                        found = True
                        if not "tables" in db:
                            db["tables"] = {}
                        if not tb_name in db["tables"]:
                            db["tables"][tb_name] = {}
                            save(server,folder)
                        tb_name = ""
                        break
                if not found:
                    page["html"] = page["html"]  +  '<div class="error">bad table name</div>'
        
        db_name = ""
        
    if action=="add_fd":
        db_name     = form.getvalue("db_name", "")
        tb_name     = form.getvalue("tb_name", "")
        fd_name     = form.getvalue("fd_name", "")
        if fd_name=="":
            page["html"] = page["html"]  +  '<div class="error">bad field name</div>'
        elif "databases" in server:
            if db_name in server["databases"]:
                db = server["databases"][db_name]
                if "tables" in db:
                    if tb_name in db["tables"]:
                        tb = db["tables"][tb_name]
                        conn = MySQLdb.connect (host = db["host"],user = db["user"], passwd = db["pass"], db = db_name)
                        cursor = conn.cursor()
                        cursor.execute("SHOW COLUMNS FROM %s LIKE '%s'"%(tb_name,fd_name))
                        field =  cursor.fetchall()
                        conn.close()
                        if len(field)==0:
                            page["html"] = page["html"]  +  '<div class="error">bad field name</div>'
                        else:
                            if not "fields" in tb:
                                tb["fields"] = {}
                            if not fd_name in tb["fields"]:
                                tb["fields"][fd_name] = ""
                                save(server,folder)
                            
                        
        tb_name = ""
        db_name = ""
    if action=="save_fd":
        db_name     = form.getvalue("db_name", "")
        tb_name     = form.getvalue("tb_name", "")
        fd_name     = form.getvalue("fd_name", "")
        fd_value     = form.getvalue("fd_value", "")
        if "databases" in server:
            if db_name in server["databases"]:
                db = server["databases"][db_name]
                if "tables" in db:
                    if tb_name in db["tables"]:
                        tb = db["tables"][tb_name]
                        if "fields" in tb:
                            if fd_name in tb["fields"]:
                                tb["fields"][fd_name] = fd_value
                                save(server,folder)                       
        db_name = ""
        tb_name = ""
        fd_name = ""
    
    if action=="delete_fd":
        db_name     = form.getvalue("db", "")
        tb_name     = form.getvalue("t", "")
        fd_name     = form.getvalue("fd", "")
        if "databases" in server:
            if db_name in server["databases"]:
                db = server["databases"][db_name]
                if "tables" in db:
                    if tb_name in db["tables"]:
                        tb = db["tables"][tb_name]
                        if "fields" in tb:
                            if fd_name in tb["fields"]:
                                del tb["fields"][fd_name]
                                save(server,folder)                       
        db_name = ""
        tb_name = ""
        fd_name = ""
        
    if action=="delete_db":
        db  =  form.getvalue("db", "")
        if db!="":
            if "databases" in server:
                if db in server["databases"]:
                    del server["databases"][db]
                    save(server,folder)
                    
    if action=="delete_table":
        db  =  form.getvalue("db", "")
        tb  =  form.getvalue("t", "")
        if db!="" and tb!="":
            if "databases" in server:
                if db in server["databases"]:
                    db = server["databases"][db]
                    if "tables" in db:
                        if tb in db["tables"]:
                            del db["tables"][tb]
                            save(server,folder)
        
    new_db_form(page)
    commands(page)
    databases(page,server)
    images(server,page)
    site_link(server,page)
    proxy(server,page);
    change_password(page)
    save_form(page)
