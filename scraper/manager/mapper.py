#!/usr/bin/python
'''
Created on 16/03/2012

@author: root
'''
from coreweb import *
lastid = 0

def load_sites_categories(sites,server):
    for i in sites:
        i.get_categories()
    for i in sites:
        for j in i.categories:
            title = j.title
            link = j.link
            site = i.site
            select_key  = {}
            select_key["%sites_category_id"] = ""
            where_keys = {}
            where_keys["%sites_category_name"] = title
            where_keys["%sites_category_site"] = site
            where_relations = {}
            where_relations["%sites_category_name"] = "="
            where_relations["%sites_category_site"] = "="
            objs,o = select(server,select_key,where=(where_keys,where_relations))
            
            if len(objs)==0:
                insert_key  = {}
                insert_key["%sites_category_site"] = site
                insert_key["%sites_category_name"] = title
                insert_key["%sites_category_link"] = link
                insert(server,insert_key)
                select_key  = {}
                select_key["%sites_category_id"] = ""
                where_keys = {}
                where_keys["%sites_category_name"] = title
                where_keys["%sites_category_site"] = site
                where_relations = {}
                where_relations["%sites_category_name"] = "="
                where_relations["%sites_category_site"] = "="
                objs,o = select(server,select_key,where=(where_keys,where_relations))
            else:
                update_key  = {}
                update_key["%sites_category_link"] = link
                where_keys = {}
                where_keys["%sites_category_name"] = title
                where_keys["%sites_category_site"] = site
                where_relations = {}
                where_relations["%sites_category_name"] = "="
                where_relations["%sites_category_site"] = "="
                update(server,update_key,where=(where_keys,where_relations))
            
            parent = objs[0][0]
            for k in j.categories:
                title = k.title
                link  = k.link
                select_key  = {}
                select_key["%sites_category_id"] = ""
                where_keys = {}
                where_keys["%sites_category_name"] = title
                where_keys["%sites_category_site"] = site
                where_keys["%sites_category_parent"] = parent
                where_relations = {}
                where_relations["%sites_category_name"] = "="
                where_relations["%sites_category_site"] = "="
                where_relations["%sites_category_parent"] = "="
                objs,o = select(server,select_key,where=(where_keys,where_relations))
                
                if len(objs)==0:
                    insert_key  = {}
                    insert_key["%sites_category_site"] = site
                    insert_key["%sites_category_name"] = title
                    insert_key["%sites_category_link"] = link
                    insert_key["%sites_category_parent"] = "%i"%(parent)
                    insert(server,insert_key)
                    
                else: 
                    update_key  = {}
                    update_key["%sites_category_link"] = link
                    where_keys = {}
                    where_keys["%sites_category_name"] = title
                    where_keys["%sites_category_site"] = site
                    where_keys["%sites_category_parent"] = parent
                    where_relations = {}
                    where_relations["%sites_category_name"] = "="
                    where_relations["%sites_category_site"] = "="
                    where_relations["%sites_category_parent"] = "="
                    update(server,update_key,where=(where_keys,where_relations))

def load_catefories_from_db(server,sites):
    for i in sites:
        select_key  = {}
        select_key["%sites_category_map"]   = ""
        select_key["%sites_category_name"]   = ""
        select_key["%sites_category_link"]  = ""
        select_key["%sites_category_parent"] = ""
        select_key["%sites_category_id"]    = ""
        where_keys = {}
        where_keys["%sites_category_site"] = i.site
        where_relations = {}
        where_relations["%sites_category_site"] = "="
        objs,order = select(server,select_key,where=(where_keys,where_relations))
        keyrel = map_rel(server,select_key)
        id = order.index(keyrel["%sites_category_id"])
        parent = order.index(keyrel["%sites_category_parent"])
        name = order.index(keyrel["%sites_category_name"])
        map = order.index(keyrel["%sites_category_map"])
        link = order.index(keyrel["%sites_category_link"])
        i.categories = []
        for j in objs:
            if j[parent]==0:
                newCat = Category(j[name],j[link])
                newCat.id = j[id]
                newCat.map = j[map]
                i.categories.append(newCat)
            else:
                for h in i.categories:
                    if j[parent]==h.id:
                        newCat = Category(j[name],j[link])
                        newCat.id = j[id]
                        newCat.map = j[map]
                        h.categories.append(newCat)
                        
         
def load_host_categories(server,page):
    select_key  = {}
    select_key["%host_category_name_read"]  = ""
    select_key["%host_category_parent_read"]  = ""
    select_key["%host_category_id_read"]  = ""
   
    objs,order = select(server,select_key)
    keyrel = map_rel(server,select_key)
    parent = order.index(keyrel["%host_category_parent_read"])
    name = order.index(keyrel["%host_category_name_read"])
    id = order.index(keyrel["%host_category_id_read"])
    page["html"] =page["html"] + "<script>"
    page["html"] =page["html"] + "var categories = new Array(" 
    cont = 0;
    parents = {}
    for i in objs:
        parents[i[id]] = i[name]
        if i[parent]==0:
            continue
        if cont == 0:
            page["html"] =page["html"] + '"%s @category:%s"'%(i[name],parents[i[parent]])
            cont = cont +1
            continue
        try:
            page["html"] =page["html"] + "," +'"%s @category:%s"'%(i[name],parents[i[parent]])
        except:
            continue

        
    page["html"] =page["html"] + ");"
    page["html"] =page["html"] + "</script>"    
def buttons(page):
    page["html"] =page["html"] + '<div class="section">'
    page["html"] =page["html"] + '<span class="title">Reload sites categories</span>'
    page["html"] =page["html"] + """
        <form class="new_db" method="GET" action="index.py">
            <input value="map"  name="p" type="hidden"/>
            <input value="l_s_c"  name="a" type="hidden"/>
            <input value="Load sites category" type="submit">
        </form>"""
    page["html"] =page["html"] + '</div>'
    
    page["html"] =page["html"] + '<form class="new_db" method="POST" action="index.py">'
    page["html"] =page["html"] + '<div class="section">'
    page["html"] =page["html"] + '<span class="title">Save map</span>'
    page["html"] =page["html"] + """
            <input value="map"  name="p" type="hidden"/>
            <input value="save"  name="a" type="hidden"/>
            <input value="Save" type="submit">
        """
    page["html"] =page["html"] + '</div>'


def print_sites(map,page,sites):
    global lastid
    for i in sites:
        page["html"] =page["html"] + '<div class="section">'
        page["html"] =page["html"] + '<span class="title">%s</span>'%(i.site)
        for j in i.categories:
            page["html"] =page["html"] + '<div class="sub_section"><span class="title">%s</span>'%(j.title)
            page["html"] =page["html"] + '<table><tbody>'
            for k in j.categories:
                help_id = str(k.id)
                value = ""
                if help_id in map:
                    value = map[help_id]
                page["html"] =page["html"] + '<tr><td style="width:450px">%s</td><td><input style="width:400px" name="%i" class="autocomplete" type="text" value="%s"/></td></tr>'%(k.title,k.id,value)
                lastid = k.id
            page["html"] =page["html"] + "</tbody></table>"
            page["html"] =page["html"] + "</div>"
            
        page["html"] =page["html"] + """<script>
                    jQuery(".autocomplete").autocomplete({
                             source: categories,
                             minLength:3
                     });
                  </script>"""
        page["html"] =page["html"] + '</div>'
        

def save_map(map,server):
    for i in map:
        text = map[i].strip()
        if len(text)>0:
            parent = re.findall("@category:(.*)",text)
            if len(parent)!=1:
                continue
            parent = parent[0]
            cat = text.replace("@category:%s"%parent,"").strip()
            select_key = {}
            select_key["%host_category_id_read"] = ""
            where_keys = {}
            where_keys["%host_category_name_read"] = parent
            where_relations = {}
            where_relations["%host_category_name_read"] = "="
            objs,o = select(server,select_key,where=(where_keys,where_relations))
            if len(objs)==0:
                continue
            parent_id = objs[0][0]
            select_key = {}
            select_key["%host_category_id_read"] = ""
            where_keys = {}
            where_keys["%host_category_name_read"] = cat
            where_keys["%host_category_parent_read"] = parent_id
            where_relations = {}
            where_relations["%host_category_name_read"] = "="
            where_relations["%host_category_parent_read"] = "="
            objs,o = select(server,select_key,where=(where_keys,where_relations))
            if len(objs)==0:
                continue
            cat_id = objs[0][0]
        else:
            cat_id = 0
        update_key  = {}
        update_key["%sites_category_map"] = cat_id
        where_keys = {}
        where_keys["%sites_category_id"] = i
        where_relations = {}
        where_relations["%sites_category_id"] = "="
        update(server,update_key,where=(where_keys,where_relations))

def read_map(server):
    map_key = {}
    select_key = {}
    select_key["%sites_category_id"] = ""
    select_key["%sites_category_map"] = ""
    objs,o = select(server,select_key)
    keyrel = map_rel(server,select_key)
    id =  o.index(keyrel["%sites_category_id"])
    map = o.index(keyrel["%sites_category_map"])
    for i in objs:
        if(i[map]==0):
            continue
        help_id = str(i[id])
        select_key = {}
        select_key["%host_category_name_read"] = ""
        select_key["%host_category_parent_read"] = ""
        where_keys = {}
        where_keys["%host_category_id_read"] = i[map]
        where_relations = {}
        where_relations["%host_category_id_read"] = "="
        query,order = select(server,select_key,where=(where_keys,where_relations))
        keyrel = map_rel(server,select_key)
        host_name       =    order.index(keyrel["%host_category_name_read"])
        host_parent     =    order.index(keyrel["%host_category_parent_read"])
        if len(query)==0:
            continue
        subcat_name   =   query[0][host_name]
        subcat_parent =   query[0][host_parent]
        select_key = {}
        select_key["%host_category_name_read"] = ""
        where_keys = {}
        where_keys["%host_category_id_read"] = subcat_parent
        where_relations = {}
        where_relations["%host_category_id_read"] = "="
        query,order = select(server,select_key,where=(where_keys,where_relations))
        if len(query)==0:
            continue
        cat_name = query[0][0]
        value = "%s @category:%s"%(subcat_name,cat_name)
        map_key[help_id]  = value
    return   map_key


    
def mapper(form,page,server,folder,sites):
    action = form.getvalue("a", None)
    map = {}
    if action=="l_s_c":
        load_sites_categories(sites,server)
    if action=="save":
        last_id = form.getvalue("last_id", "")
        try:
            last_id = int(last_id)
            if last_id>0:
                for cat in range(last_id+1):
                    help = str(cat)
                    val  = form.getvalue(help, "")
                    map[help] = val.replace("'","&apos;")
        except:
            pass                
    save_map(map,server)
    map2  = read_map(server)
    load_host_categories(server,page)
    load_catefories_from_db(server,sites)  
    buttons(page)
    print_sites(map2,page,sites)
    page["html"] =page["html"] + '<input name="last_id" type="hidden" value="%s"/>'%(lastid)  
    page["html"] =page["html"] + "</form>"
    
