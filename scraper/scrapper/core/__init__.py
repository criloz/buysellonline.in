from scrapper.BeautifulSoup import BeautifulSoup
import urllib,re,time,hashlib,MySQLdb,json
import os,random,sys
from scrapper.core.proxies import urlopen
   
bad_words = []
server = {}

def remove_attrs(soup):
    for tag in soup.findAll(True): 
        tag.attrs = None
    return soup

def get_links(text):
    links   = re.findall(" (((http(s)?|ftp):\/\/)?(([a-z0-9]*\.)?[a-z0-9]+\.[a-z]+(\.[a-z]+)?[a-zA-Z0-9\?=\+\/!#]+))",text)
    links2 = []
    for i in links:
        if i[1]=="":
            repl = "http://"+i[4]
        else:
            repl = i[0]
        links2.append(repl)
    return  links2

def change_link(text):
    links   = re.findall(" (((http(s)?|ftp):\/\/)?(([a-z0-9]*\.)?[a-z0-9]+\.[a-z]+(\.[a-z]+)?[\.a-zA-Z0-9\?=\+\/!#]+))",text)
    for i in links:
        if i[1]=="":
            repl = "http://"+i[4]
            repl2 = i[0]
        else:
            repl = i[0]
            repl2 = i[0]
        newl = "<a href='%s'>%s</a>"%(repl,repl2) 
        text = text.replace(repl2,newl)
    return  text

def find_phone_number(text):
    numbers   = re.findall("9[0-9 -]+[0-9]",text)
    if len(numbers)==0:
        return ""
    for i in numbers:
        check_number = i.replace(" ","").replace("-","")
        if len(check_number)>=9:
            return check_number
    return ""

def find_email(text):
    email   = re.findall("[A-Za-z0-9\.]+@[a-z0-9\.]+",text)
    if len(email)==0:
        return ""
    return email[0]

def remove_tags(soup):
    VALID_TAGS = 'div', 'p','b'
    for tag in soup.findAll(True):
        if tag.name not in VALID_TAGS:
            for i, x in enumerate(tag.parent.contents):
                if x == tag: break
            else:
                print "Can't find", tag, "in", tag.parent
                continue
            for r in reversed(tag.contents):
                tag.parent.insert(i, r)
            tag.extract()
    return soup.renderContents()

def remove_non_ascii(str2):
    strPattern = "[^A-Za-z 0-9 \.,\?'\"!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]"
    strReplace = ""
    return  re.sub(strPattern,strReplace,str2)
reg_email = {}
        
    
class Category():
    def __init__(self,title,link):
        self.link = link
        self.title = title
        self.only_with_images = True
        self.categories = []
        self.max_time = 60*60*24*30
        pass
    def add_category(self,title,link):
        self.categories.append(Category(title,link))
        
class Adds():
    def __init__(self,title="",description="",seller_name="",seller="",category=Category("d","d"),phone="",city="",state="",photos="",price="",date=""):
        self.user_name = ""
        self.user_fullname = ""
        self.city =""
        self.country = ""
        self.state = ""
        self.command_list = {}
        self.command_list["%user_name"] = self.user_name
        self.user_fullname = ""
        self.command_list["%user_fullname"] = self.user_fullname
        self.city = ""
        self.command_list["%city"] = self.city
        self.country = ""
        self.command_list["%country"] = self.country
        self.state = ""
        self.command_list["%state"] = self.state
        
        if description=="":
            
            self.valid = False
            return
        self.valid = True
        self.command_list = {}
        if date == "":
            date = time.mktime(time.localtime())
        email= find_email(description)
        if email=="":
            email= find_email(title)
        if phone=="":
            phone = find_phone_number(description)
            if phone =="":
                phone = find_phone_number(title)
        localtime = time.mktime(time.localtime())
        
        if  (localtime-date)>category.max_time:
            self.valid = False
        
        if len(photos)==0:
            if category.only_with_images:
                self.valid = False
        
        description = change_link(description)
        
        total = title+description+seller_name+seller+email
        bad_words  = category.bad_words
        server  = category.server
        for i in bad_words:
    	    title = title.replace(i['bad_word'],i['replace'])
    	    description = description.replace(i['bad_word'],i['replace'])
    	    if i['replace']=='%delete':
    	        if i['bad_word'] in title or i['bad_word'] in description:
    		    self.valid=False
    		    return
        self.title = title.strip()
        self.description = description.strip()
        m = hashlib.md5()
        m.update(self.title+self.description)
        seller = seller.strip()
        seller_name = seller_name.strip()
        seller=seller.lower()
        try:
            int(seller)
            seller = seller_name.replace(" ","").lower()
        except:
            pass
        if seller=="":
            if email!="":
                seller = re.findall("(.*)@",email)[0]
        if seller=="":
            if seller_name=="":
                seller = "anonymous"
            else:
                seller = seller_name.replace(" ","").lower()
        if seller=="anonymous":
            seller_name = "Anonymous"
        self.hash = m.hexdigest()
        
        self.fullname = seller_name
        self.user_name = seller
        self.phone = phone
        self.city = city
        self.state = state
        self.price = price
        m = hashlib.md5()
        self.email = email
        m.update(self.fullname.strip()+self.user_name.strip()+self.email.strip())
        self.user_hash = m.hexdigest()
        
        if self.valid==False:
            return
        self.photos = photos
        
        select_key  = {}
        select_key["%user_id"]   = ""
        where_keys = {}
        where_keys["%user_hash"] = self.user_hash
        where_relations = {}
        where_relations["%user_hash"] = "="
        objs,o = select(server,select_key,where=(where_keys,where_relations))
        help_user = ""
        if len(objs)>0:
            self.user_id = objs[0][0]
        else:
            index = 0
            help_user = self.user_name
            base_user = self.user_name
            select_key={}
            select_key["%user_name"]   = ""
            where_keys = {}
            where_keys["%user_name"] = help_user
            where_relations = {}
            where_relations["%user_name"] = " REGEXP "
            objs,o = select(server,select_key,where=(where_keys,where_relations),order=' ORDER BY id DESC LIMIT 1')
            if len(objs)>0:
		exp = re.findall(helpuser+'_([0-9]+)',objs[0][0])
		if len(exp)>0:
			index = int(exp[0])+1
                else:
			index = 1
		help_user = base_user +"_"+ str(index)
                    
            self.user_name =   help_user  
            insert_key  = {}
            insert_key["%user_hash"] = self.user_hash
            insert_key["%user_name"] = self.user_name
            insert_key["%user_full_name"] = self.fullname
            insert_key["%user_active"]   =  1
            insert_key["%user_country"]  = "india" 
            insert_key["%user_state"]    =  self.state
            insert_key["%user_email"]    =  self.email
            insert_key["%user_city"]     =  self.city
            insert_key["%user_phone"]    =  self.phone
            m = hashlib.md5()
            m.update("default")
            insert_key["%user_password"] = m.hexdigest()   
            insert(server,insert_key)
            select_key = {}
            select_key["%user_name"]   = ""
            where_keys = {}
            where_keys["%user_name"] = self.user_name
            where_relations = {}
            where_relations["%user_name"] = "="
            objs,o = select(server,select_key,where=(where_keys,where_relations))
            if len(objs)>0:
                self.user_id = objs[0][0]
            else:
                return     
        select_key  = {}
        select_key["%ads_hash"]  = ""
        where_keys = {}
        where_keys["%ads_hash"] = self.hash
        where_relations = {}
        where_relations["%ads_hash"] = "="
        objs,o = select(server,select_key,where=(where_keys,where_relations))
        
        if len(objs)>0:
            self.valid=False
            return

        #adds insertion
        insert_key  = {}
        insert_key["%ads_hash"] = self.hash
        insert_key["%ads_title"] = self.title
        insert_key["%ads_desc"] = self.description.replace('"',"&quot;").replace("&","&amp;").replace("<","&lt;").replace(">","&gt;").replace("'","&apos;")
        insert_key["%ads_duration"]   = server["ads_duration"]
        insert_key["%ads_country"]    = "india"
        insert_key["%ads_addres"]     = self.city + "," +self.state 
        insert_key["%ads_phone"]      = self.phone
        insert_key["%ads_closded"]    = 0
        insert_key["%ads_email"]      = self.email
        insert_key["%ads_start"]      = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())
        insert_key["%ads_end"]        = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(time.mktime(time.localtime()) +int(server["ads_duration"])*60))
        insert_key["%ads_cat"]        = category.map 
        insert_key["%ads_own_id"]     = self.user_id  
        insert_key["%ads_own_email"]  = self.email    
        insert_key["%ads_active"]     = 1
        insert_key["%ads_own_full_name"] = self.fullname
        insert_key["%ads_show_contact"] = 1
        insert(server,insert_key)
        select_key = {}
        select_key["%ads_id"]   = ""
        where_keys = {}
        where_keys["%ads_hash"] = self.hash
        where_relations = {}
        where_relations["%ads_hash"] = "="
        objs,o = select(server,select_key,where=(where_keys,where_relations))
        
        if(len(objs)>0):
            self.ads_id  = objs[0][0]
        else:
            return
        
        #img
        index = 0
        newphotos = []
        for i in self.photos:
            format = re.findall("\.[a-zA-Z]+$",i)
            if len(format)<1:
                continue
            format = format[0]
            map_id = category.map
            ads_id = self.ads_id
            hard = server["img_path"]
            cloud = server["img_name"]
            img_name = str(map_id)+"_"+str(ads_id)+"_"+ str(time.mktime(time.localtime())).replace(".","")+"_"+str(index) +format
            img = urlopen(server,i)
            file_img = open(hard+img_name,"wb");
            file_img.write(img)
            file_img.close()
            newphotos.append(cloud+img_name)
            index = index +1
        self.photos = newphotos
        
        if len(self.photos)>0:
            update_key  = {}
            update_key["%ads_img"] = self.photos.pop(0)
            where_keys = {}
            where_keys["%ads_id"] = self.ads_id
            where_relations = {}
            where_relations["%ads_id"] = "="
            update(server,update_key,where=(where_keys,where_relations))
            
        for j in self.photos:
            insert_key  = {}
            insert_key["%img_name"] = j
            insert_key["%img_ads_id"] = self.ads_id
            insert(server,insert_key)
        
       
        
        


def map_rel(server,keys):
    key2 = {}
    if "databases" in server:
        for db_name in server["databases"]:
            db = server["databases"][db_name]
            if "tables" in db:
                for tb_name in db["tables"]:
                    tb = db["tables"][tb_name]
                    if "fields" in tb:
                        for fd_name in tb["fields"]:
                            fd = tb["fields"][fd_name]
                            for key in keys:
                                if key in fd:
                                    key2[key] = fd_name
                    
    return key2
    
def map_dbs(server,keys):
    db_for_insertions = {}
    if "databases" in server:
        for db_name in server["databases"]:
            db = server["databases"][db_name]
            if "tables" in db:
                for tb_name in db["tables"]:
                    tb = db["tables"][tb_name]
                    if "fields" in tb:
                        for fd_name in tb["fields"]:
                            fd = tb["fields"][fd_name]
                            for key in keys:
                                if key in fd:
                                    if not db_name in  db_for_insertions:
                                        db_for_insertions[db_name] = {}
                                    if not tb_name in db_for_insertions[db_name]:
                                        db_for_insertions[db_name][tb_name] = {}
                                    if not fd_name in db_for_insertions[db_name][tb_name]:
                                        db_for_insertions[db_name][tb_name][fd_name] = fd
                                    db_for_insertions[db_name][tb_name][fd_name] = db_for_insertions[db_name][tb_name][fd_name].replace(str(key),str(keys[key]))
                    
    return db_for_insertions


def map_where(server,keys,ralations):
    key_names = map_dbs(server,keys)
    relation2 = map_dbs(server,ralations)
    where = ""
    cont = 0
    for dbname in key_names:
        for  tbname in key_names[dbname]:
            for field in key_names[dbname][tbname]:
                if cont == 0 :
                    cont = cont +1 
                    where = " where %s%s'%s'"%(field,relation2[dbname][tbname][field],key_names[dbname][tbname][field])
                    continue
                where = where + " and " + "%s%s'%s'"%(field,relation2[dbname][tbname][field],key_names[dbname][tbname][field])
            break
        break
    return where
    
def insert(server,keys):
    insertion = map_dbs(server,keys)
    for dbname in insertion:
        db = server["databases"][dbname]
        conn = MySQLdb.connect (host = db["host"],user = db["user"], passwd = db["pass"], db = dbname)
        conn.autocommit(True)
        for  tbname in insertion[dbname]:
            values = "values("
            fields = "("
            con = 0
            for field in insertion[dbname][tbname]:
                if con==0:
                    fields = fields + field
                    values = values + "'" +insertion[dbname][tbname][field] + "'"
                    con = con +1
                    continue
                fields = fields + "," + field
                values = values + "," +  "'" +insertion[dbname][tbname][field]+ "'" 
            values = values +")"
            fields = fields + ")"
            sql = "insert into %s %s %s"%(tbname,fields,values)
            print sql
            conn.cursor().execute(sql)
            break
        conn.close()
        break

def select(server,keys,**mysql):
    insertion = map_dbs(server,keys)
    ret  = ()
    orderL = []
    where = ""
    order = ""
    if "where" in mysql:
        where = map_where(server,mysql["where"][0],mysql["where"][1])
    if "order" in mysql:
	order = mysql["order"]
    for dbname in insertion:
        db = server["databases"][dbname]
        conn = MySQLdb.connect (host = db["host"],user = db["user"], passwd = db["pass"], db = dbname)
        for  tbname in insertion[dbname]:
            select = ""
            con = 0
            for field in insertion[dbname][tbname]:
                orderL.append(field)
                if con==0:
                    select = field
                    con = con +1
                    continue
                select  = select + "," + field
            sql = "select %s from %s%s%s"%(select,tbname,where,order)
            print sql
            cursor = conn.cursor()
            cursor.execute(sql)
            break
        ret = cursor.fetchall() 
        conn.close()
        break
    return ret,orderL

def update(server,keys,**mysql):
    insertion = map_dbs(server,keys)
    where = ""
    if "where" in mysql:
        where = map_where(server,mysql["where"][0],mysql["where"][1])

    for dbname in insertion:
        db = server["databases"][dbname]
        conn = MySQLdb.connect (host = db["host"],user = db["user"], passwd = db["pass"], db = dbname)
        conn.autocommit(True)
        for  tbname in insertion[dbname]:
            set2 = ""
            con = 0
            for field in insertion[dbname][tbname]:
                if con==0:
                    set2 = "set %s='%s'"%(field,insertion[dbname][tbname][field])
                    con = con +1
                    continue
                set2  = set2 + "," + "%s='%s'"%(field,insertion[dbname][tbname][field])
            sql = "update %s %s%s"%(tbname,set2,where)
            print sql
            cursor = conn.cursor()
            cursor.execute(sql)
            break
        conn.close()
        break
