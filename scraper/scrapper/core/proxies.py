'''
Created on 23/03/2012

@author: root
'''
import urllib2,time


def load_proxy_list(server):
    server["last_proxy_update"] = time.mktime(time.localtime())
    server["proxy_list"] = []
    server["good_proxy_list"] = []
    if "proxy_path" in server:
        try:
            cfile = open(server["proxy_path"])
            for line in cfile.readlines():
                server["proxy_list"] .append({"http":"http://"+line.strip()})
            cfile.close()
        except:
            pass


def urlopen(server,link):
    print link
    current_time = time.mktime(time.localtime())
    if "proxy_rtime" in server:
        if (current_time-server["last_proxy_update"])>server["proxy_rtime"]:
            load_proxy_list(server)
    if len(server["proxy_list"])==0:
        server["proxy_list"] = server["good_proxy_list"]
    if len(server["proxy_list"])==0:
        try:
	    proxy = urllib2.ProxyHandler({})
            opener = urllib2.build_opener(proxy)
            urllib2.install_opener(opener)
            page  = urllib2.urlopen(link,timeout=2)
            return page.read()
	except:
	    return ""
    else:
        proxy = urllib2.ProxyHandler(server["proxy_list"][0])
        opener = urllib2.build_opener(proxy)
        urllib2.install_opener(opener)
        try:
            page  = urllib2.urlopen(link,timeout=2)
	    #server["good_proxy_list"].append(server["proxy_list"][0])
            return page.read()
        except:
	    server["proxy_list"].pop(0) 
	    return urlopen(server,link)
