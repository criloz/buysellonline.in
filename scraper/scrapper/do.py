'''
Created on 19/03/2012

@author: root
'''
import os,sys
axd_axd = os.path.realpath(__file__).replace("/scrapper/do.py","/")
sys.path.append(axd_axd)

import cgi
from manager.coreweb import *

def application(environ,start_response):
    pythonpath = '/usr/local/python2.7/bin/python'
    script  = os.path.realpath(__file__).replace("do.py","scrap.py")
    form = cgi.FieldStorage(fp=environ['wsgi.input'], environ=environ)
    request = form.getvalue("request", None)
        
    if request!=None:
        try: 
            output = str(os.spawnv(os.P_NOWAIT,pythonpath,('python',script,request)))
            output = output + "<br>ok"
        except:
            output = "Bad Request"
    else:
        output = "NO Request"
       
    status = '200 OK'
    output =   output.encode('utf-8') 
    response_headers = [('Content-type', 'text/html; charset=utf-8'),
                        ('Content-Length', str(len(output)))]
    start_response(status, response_headers)

    return [output]

