import os,sys,urllib
axd_axd = os.path.realpath(__file__).replace("/scrapper/scrap.py","/")
os.environ['PYTHON_EGG_CACHE'] = axd_axd.replace("scraper/","")

sys.path.append(axd_axd)

import base64,json
from core import *
from sites import *


map = {"http://www.click.in":ClickIn,
       "http://www.clickindia.com":ClickIndia,
       "http://www.olx.in":Olx,
       "http://www.quikr.com":Quikr}


def load_catefories_from_db(sites,server):
    for i in sites:
        select_key  = {}
        select_key["%sites_category_map"]   = ""
        select_key["%sites_category_name"]   = ""
        select_key["%sites_category_link"]  = ""
        select_key["%sites_category_parent"] = ""
        select_key["%sites_category_id"]    = ""
        where_keys = {}
        where_keys["%sites_category_site"] = i.site
        where_keys["%sites_category_map"] = 0
        where_relations = {}
        where_relations["%sites_category_site"] = "="
        where_relations["%sites_category_map"] = "!="
        objs,order = select(server,select_key,where=(where_keys,where_relations))
        keyrel = map_rel(server,select_key)
        id = order.index(keyrel["%sites_category_id"])
        parent = order.index(keyrel["%sites_category_parent"])
        name = order.index(keyrel["%sites_category_name"])
        map = order.index(keyrel["%sites_category_map"])
        link = order.index(keyrel["%sites_category_link"])
        i.categories = []
        for j in objs:
            newCat = Category(j[name],j[link])
            newCat.id = j[id]
            newCat.map = j[map]
            i.categories.append(newCat)
            
if __name__ == "__main__":
    bad_words  = []
    server  = {}
    request = sys.argv[1]
    try: 
        request = json.loads(base64.b64decode(request))
    except:
        exit()
    sites =  request["sites"];
    categories =  request["categories"];
    site_list = []
    for i in sites:
        site_list.append(map[i]())
    
    main_folder_xxx =  os.path.realpath(__file__).replace("/scrapper/scrap.py","/manager/")  
    try:
        cfile = open(main_folder_xxx+"cfile.json")
        server  =  json.loads(cfile.read())
        cfile.close()
    except:
        server["password"] = "default"
        
    load_catefories_from_db(site_list,server)
    
    for i in site_list:
        for k in i.categories:
            k.checked = False 
            kid = str(k.map)
            if  kid in categories:
                if categories[kid]["0"][0]==0:
                    k.only_with_images = False
                else:
                    k.only_with_images = True
                k.number_of_adds   = categories[kid]["0"][1]
                k.checked = True 
                index = str(site_list.index(i))
                if index in categories[kid]:
                    if categories[kid][index][0]==0:
                        k.only_with_images = False
                    else:
                        k.only_with_images = True
                    k.number_of_adds   = categories[kid][index][1]
    
    select_key = {}
    select_key["%bad_word"]   = ""
    select_key["%replace"]   = ""
    objs,o = select(server,select_key)
    keyrel = map_rel(server,select_key)
    bw_index = o.index(keyrel["%bad_word"])
    re_index = o.index(keyrel["%replace"])
    for i in  objs:
        bad_words.append({'bad_word':i[bw_index],'replace':i[re_index]})
    if "ads_duration" in request:
        server["ads_duration"] = request["ads_duration"]
    else:
        server["ads_duration"] = 24*60*7
    server["last_proxy_update"] = 0
    server["proxy_rtime"] = int(server["proxy_rtime"])
    for i in site_list:
        for k in i.categories:
            if k.checked:
                k.server = server
                k.bad_words = bad_words
                i.get_add_form(k,k.number_of_adds)
                urllib.urlopen("http://buysellonline.in/initialize.counters.php") 
                pass 

