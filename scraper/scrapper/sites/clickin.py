'''
Created on 09/03/2012


@author: cristian criscalovis@gmail.com

'''

from scrapper.core import *
from scrapper.core.proxies import urlopen
import urllib
        
class ClickIn():
    def __init__(self):
        self.site = "http://www.click.in"
        self.categories = []
    def get_categories(self):
        try:
            content = BeautifulSoup(remove_non_ascii(urllib.urlopen(self.site).read()))
        except:
            return
        for i in content.findAll("div",attrs={"class":"clickin-indexInnerBlocks"}): 
            # get super categories
            div = i.find("div",attrs={"class":"clickin-indexHeadingContent "})
            if div==None:
                div = i.find("div",attrs={"class":"clickin-indexHeadingContent new"})
            if div==None:
                continue     
            alink = div.find("a")
            try:
                SuperCat = Category(alink.contents[0],alink["href"])
            except:
                continue
            #get sub categories
            subcat = i.find("div",attrs={"class":"clickin-indexContent"})
            links = subcat.findAll("a")
            for j in links:
                try:
                    SuperCat.add_category(j.contents[0], j["href"])
                except:
                    continue
            self.categories.append(SuperCat)
    
    def get_add_form(self,category,n):
        basepage = category.link
        page = basepage
	adds = []
        total_links = 0
        max_iteration   =  1000
        it_number = 0
        mci = 0;
        while (total_links<n):
            mci = mci + 1
	    if mci>50:
                break
            it_number = it_number + 1
            if it_number>max_iteration:
                break
            try:
                content = BeautifulSoup(remove_non_ascii(urlopen(category.server,page)))
            except:
                continue
	    ads_html = content.findAll("div",attrs={"class":"clickin-listingpagePosts"})
	    if len(ads_html)==0:
		break
            for i in ads_html:
                if category.only_with_images:
                    no_img = i.find("div",attrs={"class":"clickin-noImage"})
                    if no_img!=None:
                            continue
                div = i.find("div",attrs={"class":"clickin-postsHeading"})
                add_link  = div.find("a")
                if add_link==None:
                    continue
                try:
                    link = add_link["href"]
                    add = self.read_add(link,category)
                except:
                    add = None
                if add ==None:
                    mci = mci +1
                    continue
                mci = 0
                adds.append(add)
                total_links = total_links + 1
                if not total_links<n:
                    break
            #page = basepage + "-p-%i"%(it_number+1)
        return adds
    
    def read_add(self,link,category):
        content = BeautifulSoup(remove_non_ascii(urlopen(category.server,link)))
        p_detail =  content.find("div",attrs={"class":"clickin-postDetails"})
        if p_detail.header==None:
            return None
        if p_detail.header.h1==None:
            return None
        title =  p_detail.header.h1.contents[0]
        date = content.find("div",attrs={"class":"clickin-postedOn"})
        if date==None:
            return None
        date = date.contents[0]
        city =  content.find("div",attrs={"class":"clickin-selectCityContent"}).span.contents[0].lower()
        details =  content.find("div",attrs={"class":"clickin-adDetails"})
        if details!=None:
            li = details.findAll("li")
        else:
            li = []
        name = ""
        userId  = ""
        for i in li:
            if i.contents[0].strip()=="Contact person:":
                name = i.b.contents[0]
            if i.contents[0].strip()=="Company name:":
                name = i.b.contents[0]
    
                
        description = content.find("p",attrs={"class":"clickin-normalText"})
        
        try:
            description =  remove_tags(remove_attrs(description))
        except:
            return None
        
        telephone = find_phone_number(str(content.find("div",attrs={"id":"contactnumbers"})).replace("&nbsp;",""))
        
        try:
            fotos_list = content.find("div",attrs={"class":"clickin-adImages"}).findAll("img")
        except:
            fotos_list = []
        fotos = []
        for i in fotos_list:
            try:
                fotos.append(self.site + i["src"])
            except:
                continue
        try:
            price = content.find("div",attrs={"class":"clickin-priceText"}).span.b.contents[0].replace("Rs.","").strip()
        except:
            price = ""
        

        try:
            date =  time.mktime(time.strptime(date,"Posted on: %a, %d %b %Y, %I:%M %p"))
        except:
            return None
        state = "" 
        add =  Adds(title,description,name,userId,category,telephone,city,state,fotos,price,date)
        if add.valid == True:
            return add
        
        return None        

            
