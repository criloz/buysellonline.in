'''
Created on 10/03/2012


@author: cristian criscalovis@gmail.com

'''
from scrapper.core import *
from scrapper.core.proxies import urlopen
import urllib

class ClickIndia():

    def __init__(self):
        self.site = "http://www.clickindia.com"
        self.categories = []
        
    def get_categories(self):
        try:
            content = BeautifulSoup(remove_non_ascii(urllib.urlopen(self.site).read()))
        except:
            return
        for i in content.findAll("span",attrs={"class":"main"}):
            # get super categories
            if i.a == None:
                continue
            try:
                SuperCat = Category(i.a.contents[0],self.site+i.a["href"])
            except:
                continue
            subcontent = BeautifulSoup(remove_non_ascii(urllib.urlopen(SuperCat.link).read()))
            sub_categories = subcontent.find("div",attrs={"class":"subcategories"})
            for j in sub_categories.findAll("a"):
                try:
                    title = j.contents[0]
                    link = j["href"]
                except:
                    continue
                SuperCat.add_category(title,self.site+link)
            self.categories.append(SuperCat)
    
    def get_add_form(self,category,n):
        basepage = category.link
        page = basepage
	adds = []
        total_links = 0
        max_iteration   =  1000
        it_number = 0
        mci = 0
        while (total_links<n):
            if mci>50:
                break
            it_number = it_number + 1
            mci = mci + 1
	    if it_number>max_iteration:
                break
            try:
                content = BeautifulSoup(remove_non_ascii(urlopen(category.server,page)))
            except:
                continue
	    ads_html = content.findAll("div",attrs={"class":"adlist feat"})
	    if len(ads_html)==0:
		break
            for i in ads_html:
                if category.only_with_images:
                    img_class = i.find("img",attrs={"class":"img"})
                    if img_class==None:
                        continue
                a_link = i.find("a",attrs={"class":"cn"})
                if a_link==None:
                    continue
                try:
                    link = a_link["href"]
                except:
                    continue
                try:
                    add = self.read_add(link,category)
                except:
                    add= None
                if add ==None:
                    mci = mci + 1
                    continue
                mci = 0
                adds.append(add)
                total_links = total_links + 1
                if not total_links<n:
                    break
            page = basepage + "%i/"%(it_number+1)
        return adds
        
    def read_add(self,link,category):
        content = BeautifulSoup(remove_non_ascii(urlopen(category.server,link)))
        title =  content.find("h1",attrs={"class":"clr1 pb mr1"})
        if title==None:
            return None
        title =  remove_tags(remove_attrs(title))
        userId = content.find("p",attrs={"class":"e e2 f1 pb2 c5"}).contents[0]
        try:
            name  =   userId.contents[0]
            userId =  userId["href"].replace("/","").replace("in","")
        except:
            name = userId
            userId = userId.replace(" ","").lower()
        
        telephone = find_phone_number(str(content.find("div",attrs={"class":"flt w2_r2 mb1"})))
        
        state = ""
        city = content.find("p",attrs={"class":"city city_nav"}).contents[0].lower()
        
        description = content.find("div",attrs={"class":"lh pl1 pb ft0 e2 desc f0 clr"})
        ext = description.find("div")
        ext.extract()
        description =  remove_tags(remove_attrs(description)).replace("&nbsp;","")
        
        try:
            fotos_list = content.find("div",attrs={"class":"detailpics viewer"}).findAll("img")
        except:
            fotos_list = []
        fotos = []
        for i in fotos_list:
            try:
                fotos.append(i["src"])
            except:
                continue

        price = ""
        div = content.find("div",attrs={"class":"topborder custom_fields_box fr mt1 e"})
        if div!=None:
            p = div.findAll("p")
        for  i in p:
            span = i.findAll("span")
            if len(span)!=2:
                continue
            if span[0].contents[0]=="Price":
                price = span[1].contents[0].replace("Rs.","").strip()
        
        add =  Adds(title,description,name,userId,category,telephone,city,state,fotos,price,"")
        
        if add.valid == True:
            return add
        return None
            
