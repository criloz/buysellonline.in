#-*- coding: utf8 -*-:
'''
Created on 10/03/2012


@author: cristian criscalovis@gmail.com

'''

from scrapper.core import *
from scrapper.core.proxies import urlopen
import urllib

class Olx():

    def __init__(self):
        self.site = "http://www.olx.in"
        self.categories = []
        
    def get_categories(self):
        try:
            content = BeautifulSoup(remove_non_ascii(urllib.urlopen(self.site).read()))
        except:
            return
        for i in content.findAll("div",attrs={"class":"block"}):
            # get super categories
            h3 = i.find("h3")
            try:
                h3.a.strong.contents
                link = h3.a["href"]
            except:
                continue
            SuperCat = Category(h3.a.strong.contents[0],h3.a["href"])
            #get sub categories
            subcat = i.find("ul")
            links = subcat.findAll("a")
            for j in links:
                SuperCat.add_category(j.contents[0], j["href"])
            self.categories.append(SuperCat)
    
    def get_add_form(self,category,n):
        page = category.link
        max_iteration   =  1000
        total_links = 0
        it_number = 0
        adds = []
        mci = 0
        while (total_links<n):
            it_number = it_number + 1
            mci = mci + 1
	    if mci>50:
                break
            if it_number>max_iteration:
                break
            try:
                content = BeautifulSoup(remove_non_ascii(urlopen(category.server,page)))
            except:
                continue
	    ads_html = content.findAll("div",attrs={"class":"row clearfix"})
	    if len(ads_html)==0:
		break
            for i in ads_html:
                if category.only_with_images:
                    try:
                        img_src = i.find("a",attrs={"class":"pics-lnk"}).img
                    except:
                        continue
                    try:
                        if img_src["src"]=="":
                            continue
                    except:
                        try:
                            if img_src["delayedsrc"]=="":
                                continue
                        except:
                            continue
                h3 = i.find("h3")
                if h3==None:
                    continue
                if h3.a==None:
                    continue
                link = h3.a["href"]
                try:
                    add = self.read_add(link,category)
                except:
                    add = None
                if add ==None:
                    mci = mci + 1 
                    continue
                mci = 0;
                adds.append(add)
                total_links = total_links + 1
                if not total_links<n:
                    break
            page = page + "-p-%i"%(it_number+1)
        return adds
        
    def read_add(self,link,category):
        content = BeautifulSoup(remove_non_ascii(urlopen(category.server,link)))

        title =  content.find("p",attrs={"id":"olx_item_title"}).contents[3]
        userId = content.find("li",attrs={"class":"i-individual"}).contents[0]
        
        link2 = None
        try:
            link2 = userId["href"]
        except:
            pass
        if link2!=None:
            content2 = urlopen(category.server,link2)
            try:
                name = re.findall("First Name: <strong>([^<]*)</strong>",content2)[0]
            except:
                return None
            userId = userId.contents[0]
        else:
            name = ""
        try:
            telephone = content.find("li",attrs={"class":"i-tel"}).contents[0]
        except:
            telephone=""
        item_data = content.find("div",attrs={"id":"item-data"})
        li = item_data.findAll("li")
        date = ""
        location = ""
        phone2 = ""
        for i in li:
            if i.contents[0].strip()=="Date Posted:":
                date = i.strong.contents[0]
            if i.contents[0].strip()=="Location:":
                location = i.strong.contents[0]
            if i.contents[0].strip()=="Phone:":
                phone2 = i.strong.contents[0]
        
        if date=="":
            return
        
        if location =="":
            return
        
        if  phone2!="":
            if telephone=="":
                telephone = phone2
                
        description = content.find("div",attrs={"id":"description-text"})
        description =  remove_tags(remove_attrs(description))
        date = str(time.localtime().tm_year) + " " + date
        location =  location.split(",")
        if len(location)<3:
            return None
        city = location[0].strip()
        state = location[1].strip()
        try:
            fotos_list = content.find("ul",attrs={"id":"big-thumbs-list"}).findAll("a")
        except:
            fotos_list = []
        fotos = []
        for i in fotos_list:
            try:
                fotos.append(i["href"])
            except:
                continue
        try:
            price = content.find("strong",attrs={"class":"price"}).contents[0].replace("Rs","").strip()
        except:
            price = ""
            
        date =  time.mktime(time.strptime(date,"%Y %B %d"))
        add =  Adds(title,description,name,userId,category,telephone,city,state,fotos,price,date)
        if add.valid == True:
            return add
        return None
    


