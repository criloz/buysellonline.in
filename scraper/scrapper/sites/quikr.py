'''
Created on 10/03/2012

@author: cristian
'''
from scrapper.core import *
from scrapper.core.proxies import urlopen
import urllib

class Quikr():

    def __init__(self):
        self.site = "http://www.quikr.com"
        self.categories = []
        
    def get_categories(self):
        try:
            content = BeautifulSoup(remove_non_ascii(urllib.urlopen(self.site).read()))
        except:
            return None
        
        for i in content.findAll("div",attrs={"id":"cats"}):
            # get super categories
            h2 = i.find("h2")
            if h2==None:
                continue
            if h2.a==None:
                continue     
            SuperCat = Category(h2.a.contents[0],self.site+h2.a["href"])
            #get sub categories
            subcat = i.find("div",attrs={"class":"scroll"})
            links = subcat.findAll("a")
            for j in links:
                SuperCat.add_category(j.contents[0], self.site+j["href"])
            self.categories.append(SuperCat)
    
    def get_add_form(self,category,n):
        page = category.link
        adds = []
        max_iteration   =  1000
        total_links = 0
        it_number = 0
        mci = 0
        while (total_links<n):
            mci = mci + 1
	    if mci>50:
                break
            it_number = it_number + 1
            if it_number>max_iteration:
                break
            try:
                content = BeautifulSoup(remove_non_ascii(urlopen(category.server,page)))
            except:
                continue
            link = ""
	    ads_html = content.findAll("div",attrs={"class":"ad"})
	    if len(ads_html)==0:
		break
            for i in ads_html:
                a_links = i.findAll("a")
                for j in a_links:
                    try:
                        t = j["target"]
                        link = j["href"]
                    except:
                        continue
                try:
                    add = self.read_add(link,category)
                except:
                    add = None
                if add ==None:
                    mci = mci +1
                    continue
                mci = 0
                adds.append(add)
                total_links = total_links + 1
                if not total_links<n:
                    break
            page = page + "?page=%i"%(it_number+1)
        return adds
        
    def read_add(self,link,category):
        content = BeautifulSoup(remove_non_ascii(urlopen(category.server,link)))
        title =  content.find("div",attrs={"id":"welcome"})
        if title.script!=None:
            title.script.extract()
        title = remove_tags(remove_attrs(title)).replace("&amp;","").strip()
        userId = ""
        name = ""
        telephone = ""
        city = content.find("div",attrs={"class":"select_city_sub"}).a.contents[0].lower()
        state = ""
        col = content.findAll("div",attrs={"class":"col3_div"})
        price = ""
        date = ""
        for i in col:
            if len(i.findAll("span"))!=2:
                continue
            if i.span.contents[0]=="Last Posted On:":
                date = i.findAll("span")[1].contents[0].strip()
            if i.span.contents[0]=="Price:":
                price = i.findAll("span")[1].contents[0].replace("Rs.","").strip()
       
          
        description = content.find("div",attrs={"class":"vapdealdesc"})
        description =  remove_tags(remove_attrs(description))
         
        try:
            fotos_list = content.find("table",attrs={"class":"image_vap"}).findAll("a")
        except:
            fotos_list = []
        fotos = []
        for i in fotos_list:
            try:
                fotos.append(self.site+i["href"])
            except:
                continue
        fotos2  = []
        for j in fotos:
            content2 = BeautifulSoup(remove_non_ascii(urlopen(category.server,j)))
            div  = content2.find("div",attrs={"id":"img"})
            if div!=None:
                if div.img!=None:
                    fotos2.append(div.img["src"])
        fotos = fotos2   
        date =  ""
        add =  Adds(title,description,name,userId,category,telephone,city,state,fotos,price,date)
        if add.valid == True:
            return add
        return None
